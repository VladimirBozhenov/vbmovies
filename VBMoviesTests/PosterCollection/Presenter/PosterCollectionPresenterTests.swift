//
//  PosterCollection
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class PosterCollectionPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: PosterCollectionInteractorInput {

    }

    class MockRouter: PosterCollectionRouterInput {

    }

    class MockViewController: PosterCollectionViewInput {

        func setupInitialState() {

        }
    }
}
