//
//  PosterCollection
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class PosterCollectionModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = PosterCollectionViewControllerMock()
        let configurator = PosterCollectionModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "PosterCollectionView is nil after configuration")
        XCTAssertTrue(viewController.output is PosterCollectionPresenter, "output is not PosterCollectionPresenter")

        let presenter: PosterCollectionPresenter = viewController.output as! PosterCollectionPresenter
        XCTAssertNotNil(presenter.view, "view in PosterCollectionPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PosterCollectionPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PosterCollectionRouter, "router is not PosterCollectionRouter")

        let interactor: PosterCollectionInteractor = presenter.interactor as! PosterCollectionInteractor
        XCTAssertNotNil(interactor.output, "output in PosterCollectionInteractor is nil after configuration")
    }

    class PosterCollectionViewControllerMock: PosterCollectionViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
