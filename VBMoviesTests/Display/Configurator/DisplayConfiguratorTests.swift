//
//  Display
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class DisplayModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = DisplayViewControllerMock()
        let configurator = DisplayModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "DisplayView is nil after configuration")
        XCTAssertTrue(viewController.output is DisplayPresenter, "output is not DisplayPresenter")

        let presenter: DisplayPresenter = viewController.output as! DisplayPresenter
        XCTAssertNotNil(presenter.view, "view in DisplayPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in DisplayPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is DisplayRouter, "router is not DisplayRouter")

        let interactor: DisplayInteractor = presenter.interactor as! DisplayInteractor
        XCTAssertNotNil(interactor.output, "output in DisplayInteractor is nil after configuration")
    }

    class DisplayViewControllerMock: DisplayViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
