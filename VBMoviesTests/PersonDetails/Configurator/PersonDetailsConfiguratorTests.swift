//
//  PersonDetails
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class PersonDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = PersonDetailsViewControllerMock()
        let configurator = PersonDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "PersonDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is PersonDetailsPresenter, "output is not PersonDetailsPresenter")

        let presenter: PersonDetailsPresenter = viewController.output as! PersonDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in PersonDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PersonDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PersonDetailsRouter, "router is not PersonDetailsRouter")

        let interactor: PersonDetailsInteractor = presenter.interactor as! PersonDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in PersonDetailsInteractor is nil after configuration")
    }

    class PersonDetailsViewControllerMock: PersonDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
