//
//  ProfileTabController
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class ProfileTabControllerModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ProfileTabControllerViewControllerMock()
        let configurator = ProfileTabControllerModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ProfileTabControllerViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ProfileTabControllerPresenter, "output is not ProfileTabControllerPresenter")

        let presenter: ProfileTabControllerPresenter = viewController.output as! ProfileTabControllerPresenter
        XCTAssertNotNil(presenter.view, "view in ProfileTabControllerPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ProfileTabControllerPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ProfileTabControllerRouter, "router is not ProfileTabControllerRouter")

        let interactor: ProfileTabControllerInteractor = presenter.interactor as! ProfileTabControllerInteractor
        XCTAssertNotNil(interactor.output, "output in ProfileTabControllerInteractor is nil after configuration")
    }

    class ProfileTabControllerViewControllerMock: ProfileTabControllerViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
