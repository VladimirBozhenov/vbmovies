//
//  MovieListMovieListConfiguratorTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class MovieListModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = MovieListViewControllerMock()
        let configurator = MovieListModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "MovieListView is nil after configuration")
        XCTAssertTrue(viewController.output is MovieListPresenter, "output is not MovieListPresenter")

        let presenter: MovieListPresenter = viewController.output as! MovieListPresenter
        XCTAssertNotNil(presenter.view, "view in MovieListPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in MovieListPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is MovieListRouter, "router is not MovieListRouter")

        let interactor: MovieListInteractor = presenter.interactor as! MovieListInteractor
        XCTAssertNotNil(interactor.output, "output in MovieListInteractor is nil after configuration")
    }

    class MovieListViewControllerMock: MovieListViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
