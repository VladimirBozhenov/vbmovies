//
//  MovieDetailsMovieDetailsConfiguratorTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class MovieDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = MovieDetailsViewControllerMock()
        let configurator = MovieDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "MovieDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is MovieDetailsPresenter, "output is not MovieDetailsPresenter")

        let presenter: MovieDetailsPresenter = viewController.output as! MovieDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in MovieDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in MovieDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is MovieDetailsRouter, "router is not MovieDetailsRouter")

        let interactor: MovieDetailsInteractor = presenter.interactor as! MovieDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in MovieDetailsInteractor is nil after configuration")
    }

    class MovieDetailsViewControllerMock: MovieDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
