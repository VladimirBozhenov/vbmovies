//
//  PosterPosterConfiguratorTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class PosterModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = PosterViewControllerMock()
        let configurator = PosterModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "PosterView is nil after configuration")
        XCTAssertTrue(viewController.output is PosterPresenter, "output is not PosterPresenter")

        let presenter: PosterPresenter = viewController.output as! PosterPresenter
        XCTAssertNotNil(presenter.view, "view in PosterPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PosterPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PosterRouter, "router is not PosterRouter")

        let interactor: PosterInteractor = presenter.interactor as! PosterInteractor
        XCTAssertNotNil(interactor.output, "output in PosterInteractor is nil after configuration")
    }

    class PosterViewControllerMock: PosterViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
