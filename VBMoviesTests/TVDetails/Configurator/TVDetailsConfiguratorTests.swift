//
//  TVDetails
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class TVDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TVDetailsViewControllerMock()
        let configurator = TVDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TVDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TVDetailsPresenter, "output is not TVDetailsPresenter")

        let presenter: TVDetailsPresenter = viewController.output as! TVDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in TVDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TVDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TVDetailsRouter, "router is not TVDetailsRouter")

        let interactor: TVDetailsInteractor = presenter.interactor as! TVDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in TVDetailsInteractor is nil after configuration")
    }

    class TVDetailsViewControllerMock: TVDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
