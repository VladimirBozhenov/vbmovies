//
//  IconCollection
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class IconCollectionModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = IconCollectionViewControllerMock()
        let configurator = IconCollectionModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "IconCollectionView is nil after configuration")
        XCTAssertTrue(viewController.output is IconCollectionPresenter, "output is not IconCollectionPresenter")

        let presenter: IconCollectionPresenter = viewController.output as! IconCollectionPresenter
        XCTAssertNotNil(presenter.view, "view in IconCollectionPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in IconCollectionPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is IconCollectionRouter, "router is not IconCollectionRouter")

        let interactor: IconCollectionInteractor = presenter.interactor as! IconCollectionInteractor
        XCTAssertNotNil(interactor.output, "output in IconCollectionInteractor is nil after configuration")
    }

    class IconCollectionViewControllerMock: IconCollectionViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
