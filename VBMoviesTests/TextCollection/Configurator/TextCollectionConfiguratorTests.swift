//
//  TextCollection
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class TextCollectionModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TextCollectionViewControllerMock()
        let configurator = TextCollectionModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TextCollectionView is nil after configuration")
        XCTAssertTrue(viewController.output is TextCollectionPresenter, "output is not TextCollectionPresenter")

        let presenter: TextCollectionPresenter = viewController.output as! TextCollectionPresenter
        XCTAssertNotNil(presenter.view, "view in TextCollectionPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TextCollectionPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TextCollectionRouter, "router is not TextCollectionRouter")

        let interactor: TextCollectionInteractor = presenter.interactor as! TextCollectionInteractor
        XCTAssertNotNil(interactor.output, "output in TextCollectionInteractor is nil after configuration")
    }

    class TextCollectionViewControllerMock: TextCollectionViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
