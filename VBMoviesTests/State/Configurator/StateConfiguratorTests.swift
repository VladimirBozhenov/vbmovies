//
//  StateStateConfiguratorTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class StateModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = StateViewControllerMock()
        let configurator = StateModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "StateView is nil after configuration")
        XCTAssertTrue(viewController.output is StatePresenter, "output is not StatePresenter")

        let presenter: StatePresenter = viewController.output as! StatePresenter
        XCTAssertNotNil(presenter.view, "view in StatePresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in StatePresenter is nil after configuration")
        XCTAssertTrue(presenter.router is StateRouter, "router is not StateRouter")
    }

    class StateViewControllerMock: StateViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
