//
//  TextTextConfiguratorTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class TextModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TextViewControllerMock()
        let configurator = TextModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TextView is nil after configuration")
        XCTAssertTrue(viewController.output is TextPresenter, "output is not TextPresenter")

        let presenter: TextPresenter = viewController.output as! TextPresenter
        XCTAssertNotNil(presenter.view, "view in TextPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TextPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TextRouter, "router is not TextRouter")

        let interactor: TextInteractor = presenter.interactor as! TextInteractor
        XCTAssertNotNil(interactor.output, "output in TextInteractor is nil after configuration")
    }

    class TextViewControllerMock: TextViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
