//
//  TextTextPresenterTests.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import XCTest

class TextPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: TextInteractorInput {

    }

    class MockRouter: TextRouterInput {

    }

    class MockViewController: TextViewInput {

        func setupInitialState() {

        }
    }
}
