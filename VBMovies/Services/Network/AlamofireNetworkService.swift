//
//  AlamofireNetworkService.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Alamofire

class AlamofireNetworkService: NetworkService {
  
  private let baseUrl = URL(string: Environment.serverURL)!
  
  func fetchData<T: Codable>(of type: T.Type,
                             path: Path,
                             appendix: String?,
                             ending: String?,
                             page: String?,
                             completion: @escaping (Result<T, Error>) -> Void) {
    let fullUrl = baseUrl.appendingPathComponent(path.rawValue + (appendix ?? "") + (ending ?? ""))
    var parameters: Parameters? {
      return [
        "api_key": Environment.tmdbAPIKey,
        "language": "ru-RU",
        "region": "RU",
        "page": page ?? ""
      ]
    }
    AF.request(fullUrl,
               method: .get,
               parameters: parameters)
      .validate()
      .responseDecodable(of: T.self,
                         queue: .global()) { response in
                          switch response.result {
                          case .success(let data):
                            completion(.success(data))
                          case .failure(let error):
                            completion(.failure(error))
                          }
    }
  }
}
