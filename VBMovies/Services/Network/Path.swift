//
//  Path.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

enum Path: String {
    case movieTop = "/3/movie/top_rated"
    case moviePopular = "/3/movie/popular"
    case movieUpcoming = "/3/movie/upcoming"
    case movieNowPlaying = "/3/movie/now_playing"
    case tvTop = "/3/tv/top_rated"
    case tvPopular = "/3/tv/popular"
    case tvOnTheAir = "/3/tv/on_the_air"
    case tvAiringToday = "/3/tv/airing_today"
    
    case movieDetails = "/3/movie/"
    case tvDetails = "/3/tv/"
    
    case personDetails = "/3/person/"
}
