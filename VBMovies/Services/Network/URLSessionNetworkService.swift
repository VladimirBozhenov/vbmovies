//
//  URLSessionNetworkService.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class URLSessionNetworkService: NetworkService {
  
  private let baseUrl = URL(string: Environment.serverURL)!
  
  private let defaultSession = URLSession(configuration: .default)
  private var dataTask: URLSessionDataTask?
  
  func fetchData<T: Codable>(of type: T.Type,
                             path: Path,
                             appendix: String?,
                             ending: String?,
                             page: String?,
                             completion: @escaping (Result<T, Error>) -> Void) {
    
    dataTask?.cancel()
    
    if var urlComponents = URLComponents(string: Environment.serverURL) {
      urlComponents.path = path.rawValue + (appendix ?? "") + (ending ?? "")
      urlComponents.queryItems = [
        URLQueryItem(name: "api_key", value: Environment.tmdbAPIKey),
        URLQueryItem(name: "language", value: "ru-RU"),
        URLQueryItem(name: "region", value: "RU"),
        URLQueryItem(name: "page", value: page ?? "")
      ]
      guard let url = urlComponents.url else {
        return
      }
      DispatchQueue.global().async {
        self.dataTask = self.defaultSession.dataTask(with: url) { [weak self] data, _, error in
          guard let self = self else { return }
          defer {
            self.dataTask = nil
          }
          if let error = error {
            completion(.failure(error))
          } else if let data = data {
            guard let value = try? JSONDecoder().decode(T.self, from: data) else { return }
            completion(.success(value))
          }
        }
        self.dataTask?.resume()
      }
    }
  }
}
