//
//  NetworkService.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol NetworkService {
  func fetchData<T: Codable>(of type: T.Type,
                             path: Path,
                             appendix: String?,
                             ending: String?,
                             page: String?,
                             completion: @escaping (Result<T, Error>) -> Void)
}
