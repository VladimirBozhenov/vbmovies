//
//  Environments.swift
//  VBMovieDB-VIPER
//
//  Created by Vladimir Bozhenov on 04/10/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

public enum Environment {
  enum Keys {
    enum Plist {
      static let tmdbAPIKey: String = "TMDB API"
      static let serverURL: String = "Server URL"
      static let imageURL: String = "Image URL"
      static let flagURL: String = "Flag URL"
      static let flagEnding: String = "Flag ending"
    }
  }
  private static let infoDictionary: [String: Any] = {
    guard let dict = Bundle.main.infoDictionary else {
      fatalError()
    }
    return dict
  }()
  
  static let tmdbAPIKey: String = {
    guard let key = Environment.infoDictionary[Keys.Plist.tmdbAPIKey] as? String else {
      fatalError()
    }
    return key
  }()
  
  static let serverURL: String = {
    guard let key = Environment.infoDictionary[Keys.Plist.serverURL] as? String else {
      fatalError()
    }
    return key
  }()
  
  static let imageURL: String = {
    guard let key = Environment.infoDictionary[Keys.Plist.imageURL] as? String else {
      fatalError()
    }
    return key
  }()
  static let flagURL: String = {
    guard let key = Environment.infoDictionary[Keys.Plist.flagURL] as? String else {
      fatalError()
    }
    return key
  }()
  static let flagEnding: String = {
    guard let key = Environment.infoDictionary[Keys.Plist.flagEnding] as? String else {
      fatalError()
    }
    return key
  }()
}
