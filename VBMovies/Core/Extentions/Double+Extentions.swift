//
//  DoubleExtentions.swift
//  VBMovieDB-VIPER
//
//  Created by Vladimir Bozhenov on 27/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

extension Double {
    func toVoteFormat() -> String {
        return String(Int(self * 10))
    }
}
