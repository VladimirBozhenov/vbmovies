//
//  StringExtentions.swift
//  VBMovieDB-VIPER
//
//  Created by Vladimir Bozhenov on 27/09/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

enum Format: String {
    case dataFormat = "dd MMM yyyy"
    case yearFormat = "yyyy"
}

extension String {
    func toFormat (_ format: Format) -> String {
        let dateFormaterFrom = DateFormatter()
        dateFormaterFrom.dateFormat = "yyyy-MM-dd"
        let dateFormaterTo = DateFormatter()
        dateFormaterTo.dateFormat = format.rawValue
        dateFormaterTo.locale = Locale(identifier: "ru-RU")
        guard let date = dateFormaterFrom.date(from: self) else { return "----"}
        return dateFormaterTo.string(from: date)
    }
}

extension String {
    
    private static var colorCache: [String: UIColor] = [:]
    
    func calculateColorFromRating() -> UIColor {
        let data = CGFloat(Double(self) ?? 0)
        let key = self
        
        if let cachedColor = String.colorCache[key] {
            return cachedColor
        }
        
        String.clearColorCacheIfNeeded()
        
        let red = data <= 50 ? 1 : (100 - data) / 50
        let green = data <= 50 ? data / 50 : 1
        let blue = CGFloat(0)
        
        let color = UIColor(red: red,
                            green: green,
                            blue: blue,
                            alpha: 1)
        
        String.colorCache[key] = color
        
        return color
    }
    
    private static func clearColorCacheIfNeeded() {
        let maxObjectsCount = 100
        guard colorCache.count >= maxObjectsCount else { return }
        colorCache = [:]
    }
}
