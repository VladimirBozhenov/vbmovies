//
//  UIColor+Extentions.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

extension UIFont {
  
  static let movieTitleFont = UIFont.systemFont(ofSize: 20,
                                                weight: .bold)
  static let movieVoteFont = UIFont.systemFont(ofSize: 10,
                                               weight: .bold)
  static let movieReleaseFont = UIFont.systemFont(ofSize: 12,
                                                  weight: .light)
  static let genresFont = UIFont.systemFont(ofSize: 15,
                                            weight: .bold)
  static let movieOverviewFont = UIFont.systemFont(ofSize: 12,
                                                   weight: .light)
  static let movieGenreFont = UIFont.systemFont(ofSize: 12,
                                                weight: .light)
  static let topMenuFont = UIFont.systemFont(ofSize: 14,
                                             weight: .medium)
  
  enum Font: String {
    case SFUIText
    case SFUIDisplay
  }
  
  private static func name(of weight: UIFont.Weight) -> String? {
    switch weight {
    case .ultraLight: return "UltraLight"
    case .thin: return "Thin"
    case .light: return "Light"
    case .regular: return nil
    case .medium: return "Medium"
    case .semibold: return "Semibold"
    case .bold: return "Bold"
    case .heavy: return "Heavy"
    case .black: return "Black"
    default: return nil
    }
  }
  
  convenience init?(font: Font, weight: UIFont.Weight, size: CGFloat) {
    var fontName = ".\(font.rawValue)"
    if let weightName = UIFont.name(of: weight) { fontName += "-\(weightName)" }
    self.init(name: fontName, size: size)
  }
  
}
