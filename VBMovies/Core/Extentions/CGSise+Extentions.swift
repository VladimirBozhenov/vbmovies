//
//  CGSise+Extentions.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 26.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

extension CGSize {
    
    static let posterSize = CGSize(width: 120,
                                   height: 170)
    static let backgroundSize = CGSize(width: 40,
                                       height: 40)
    static let iconSise = CGSize(width: 70,
                                 height: 50)
    
}
