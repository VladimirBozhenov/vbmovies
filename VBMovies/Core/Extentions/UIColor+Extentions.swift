//
//  UIColor+Extentions.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 26.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let topMainColor = UIColor.white
    static let detailsMainColor = UIColor.darkGray
    static let textMainColor = UIColor.black
    static let textWhiteColor = UIColor.white
}
