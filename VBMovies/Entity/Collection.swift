//
//  Collection.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Collection: Codable {
    let collectionID: Int
    let collectionName: String
    let overview: String?
    let posterPath: String?
    let backdropPath: String?
    let parts: [MovieAndTV]?
    
    enum CodingKeys: String, CodingKey {
        case collectionID = "id"
        case collectionName = "name"
        case overview
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case parts
    }
}
