//
//  PosterViewModel.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol PosterViewModel {
    var itemID: Int { get set }
    var itemPoster: URL? { get set }
    var itemDescription: String { get set }
    var mediaType: String { get set }
}

struct MoviePosterModel: PosterViewModel {
    var itemID: Int
    var itemPoster: URL?
    var itemDescription: String
    var mediaType: String

    init(using item: MovieAndTV) {
        self.itemID = item.itemID
        if let posterPath = item.posterPath {
            self.itemPoster = URL(string: Environment.imageURL + posterPath)
        } else {
            self.itemPoster = URL(fileURLWithPath: "placeholder")
        }
        self.itemDescription = item.title ?? item.name ?? ""
        self.mediaType = item.mediaType ?? ""
    }
}

struct PersonPosterModel: PosterViewModel {
    var itemID: Int
    var itemPoster: URL?
    var itemDescription: String
    var mediaType: String
    
    init(using person: Person) {
        self.itemID = person.personID
        if let profilePath = person.profilePath {
            self.itemPoster = URL(string: Environment.imageURL + profilePath)
        } else {
            self.itemPoster = URL(fileURLWithPath: "placeholder")
        }
        self.itemDescription = person.character != nil ? "\(person.name)\n\n\(person.character ?? "")" : "\(person.name)\n\n\(person.job ?? "")"
        self.mediaType = "person"
    }
}
