//
//  MovieDetailsViewModel.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.

import UIKit

struct MovieDetailsViewModel {
  let genres: [String]
  let movieTitle: String
  let movieImage: URL?
  let backdropImage: URL?
  let overview: String?
  let companies: [URL]?
  let countriesFlag: [URL]?
  let releaseDate: String
  let runtime: String
  let voteAverage: String
  let voteBorderColor: CGColor
  
  init(using movieDetails: MovieDetails) {
    self.genres = movieDetails.genres
      .map { $0.genreName }
    self.movieTitle = movieDetails.title
    if let posterPath = movieDetails.posterPath {
      self.movieImage = URL(string: Environment.imageURL + posterPath)
    } else {
      self.movieImage = URL(fileURLWithPath: "placeholder")
    }
    if let backdropPath = movieDetails.backdropPath {
      self.backdropImage = URL(string: Environment.imageURL + backdropPath)
    } else {
      self.backdropImage = URL(fileURLWithPath: "placeholder")
    }
    self.overview = movieDetails.overview
    self.companies = movieDetails.productionCompanies
      .filter { $0.logoPath != nil }
      .map { URL(string: Environment.imageURL + $0.logoPath!)! }
    self.countriesFlag = movieDetails.productionCountries
      .map { URL(string: Environment.flagURL + $0.iso + Environment.flagEnding)! }
    self.releaseDate = "• \(movieDetails.releaseDate.toFormat(.yearFormat)) •"
    self.runtime = "\(movieDetails.runtime ?? 0) min."
    self.voteAverage = "\(movieDetails.voteAverage.toVoteFormat())%"
    self.voteBorderColor = movieDetails.voteAverage.toVoteFormat().calculateColorFromRating().cgColor
  }
}
