//
//  TVDetailsViewModel.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import UIKit

struct TVDetailsViewModel {
    let genres: [String]
    let tvTitle: String
    let tvImage: URL?
    let backdropImage: URL?
    let overview: String?
    let companies: [URL]?
    let networks: [URL]?
    let releaseDate: String
    let voteAverage: String
    let voteBorderColor: CGColor
    
    init(using tvDetails: TVDetails) {
        self.genres = tvDetails.genres
            .map { $0.genreName }
        self.tvTitle = tvDetails.name
        if let posterPath = tvDetails.posterPath {
            self.tvImage = URL(string: Environment.imageURL + posterPath)
        } else {
            self.tvImage = URL(fileURLWithPath: "placeholder")
        }
        if let backdropPath = tvDetails.backdropPath {
            self.backdropImage = URL(string: Environment.imageURL + backdropPath)
        } else {
            self.backdropImage = URL(fileURLWithPath: "placeholder")
        }
        self.overview = tvDetails.overview
        self.companies = tvDetails.productionCompanies
            .filter { $0.logoPath != nil }
            .map { URL(string: Environment.imageURL + $0.logoPath!)! }
        self.networks = tvDetails.networks
            .filter { $0.logoPath != nil }
            .map { URL(string: Environment.imageURL + $0.logoPath!)! }
        self.releaseDate = "• \(tvDetails.firstAirDate.toFormat(.yearFormat)) •"
        self.voteAverage = "\(tvDetails.voteAverage.toVoteFormat())%"
        self.voteBorderColor = tvDetails.voteAverage.toVoteFormat().calculateColorFromRating().cgColor
    }
}
