//
//  MovieAndTVListViewModel.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 17.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

struct MovieAndTVListViewModel {
    
    let itemID: Int
    let mediaType: String
    let movieTitle: String?
    let movieImage: URL?
    let overview: String
    let releaseDate: String?
    let voteAverage: String
    let voteBorderColor: CGColor
    
    init(using item: MovieAndTV) {
        self.itemID = item.itemID
        if let mediaType = item.mediaType {
            self.mediaType = mediaType
        } else {
            self.mediaType = item.title != nil ? "movie" : "tv"
        }
        self.movieTitle = item.title == nil ? item.name : item.title
        if let posterPath = item.posterPath {
            self.movieImage = URL(string: Environment.imageURL + posterPath)
        } else {
            self.movieImage = URL(fileURLWithPath: "placeholder")
        }
        self.overview = item.overview
        self.releaseDate = item.releaseDate == nil ? item.firstAirDate?.toFormat(.dataFormat) : item.releaseDate?.toFormat(.dataFormat)
        self.voteAverage = item.voteAverage.toVoteFormat() + "%"
        self.voteBorderColor = item.voteAverage.toVoteFormat().calculateColorFromRating().cgColor
    }
}
