//
//  PersonDetailsViewModel.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

struct PersonDetailsViewModel {
    
    let birthday: String?
    let deathday: String?
    let personID: Int
    let name: String
    let biography: String
    let popularity: String
    let placeOfBirth: String?
    let profilePath: URL?
    let popularityBorderColor: CGColor
    
    init(using person: PersonDetails) {
        self.birthday = person.birthday?.toFormat(.dataFormat)
        self.deathday = person.deathday?.toFormat(.dataFormat)
        self.personID = person.personID
        self.name = person.name
        self.biography = person.biography
        self.popularity = person.popularity.toVoteFormat() + "%"
        self.placeOfBirth = person.placeOfBirth
        if let profilePath = person.profilePath {
            self.profilePath = URL(string: Environment.imageURL + profilePath)
        } else {
            self.profilePath = URL(fileURLWithPath: "placeholder")
        }
        self.popularityBorderColor = person.popularity.toVoteFormat().calculateColorFromRating().cgColor
    }
}
