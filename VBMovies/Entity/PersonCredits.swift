//
//  PersonCredits.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct PersonCredits: Codable {
    let personID: Int
    let castIn: [MovieAndTV]
    let crewIn: [MovieAndTV]
    
    enum CodingKeys: String, CodingKey {
        case personID = "id"
        case castIn = "cast"
        case crewIn = "crew"
    }
}
