//
//  Language.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Language: Codable {
  let iso: String
  let languageName: String
  
  enum CodingKeys: String, CodingKey {
    case iso = "iso_639_1"
    case languageName = "name"
  }
}
