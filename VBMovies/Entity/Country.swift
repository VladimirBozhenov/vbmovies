//
//  Country.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Country: Codable {
  let iso: String
  let countryName: String
  
  enum CodingKeys: String, CodingKey {
    case iso = "iso_3166_1"
    case countryName = "name"
  }
}
