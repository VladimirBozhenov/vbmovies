//
//  Company.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Company: Codable {
    let companyName: String
    let companyID: Int
    let logoPath: String?
    let originCountry: String
    
    enum CodingKeys: String, CodingKey {
        case companyName = "name"
        case companyID = "id"
        case logoPath = "logo_path"
        case originCountry = "origin_country"
    }
}
