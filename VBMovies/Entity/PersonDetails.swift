//
//  PersonDetails.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct PersonDetails: Codable {
    let birthday: String?
    let knownForDepartment: String
    let deathday: String?
    let personID: Int
    let name: String
    let alsoKnownAs: [String]
    let gender: Int
    let biography: String
    let popularity: Double
    let placeOfBirth: String?
    let profilePath: String?
    let adult: Bool
    let imdbID: String
    let homepage: String?
    
    enum CodingKeys: String, CodingKey {
        case birthday
        case knownForDepartment = "known_for_department"
        case deathday
        case personID = "id"
        case name
        case alsoKnownAs = "also_known_as"
        case gender
        case biography
        case popularity
        case placeOfBirth = "place_of_birth"
        case profilePath = "profile_path"
        case adult
        case imdbID = "imdb_id"
        case homepage
    }
}
