//
//  TVDetails.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct TVDetails: Codable {
    let backdropPath: String?
    let episodeRuntime: [Int]
    let firstAirDate: String
    let genres: [Genre]
    let homepage: String
    let inProduction: Bool
    let languages: [String]
    let lastAirDate: String
    let name: String
    let networks: [Networks]
    let numberOfEpisodes: Int
    let numberOfSeasons: Int
    let originCountry: [String]
    let originalLanguage: String
    let originalName: String
    let overview: String?
    let popularity: Double
    let posterPath: String?
    let productionCompanies: [Company]
    let seasons: [Season]
    let status: String
    let tvID: Int
    let voteAverage: Double
    let voteCount: Int
    
    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case episodeRuntime = "episode_run_time"
        case firstAirDate = "first_air_date"
        case genres
        case homepage
        case inProduction = "in_production"
        case languages
        case lastAirDate = "last_air_date"
        case name
        case networks
        case numberOfEpisodes = "number_of_episodes"
        case numberOfSeasons = "number_of_seasons"
        case originCountry = "origin_country"
        case originalLanguage = "original_language"
        case originalName = "original_name"
        case overview
        case popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case seasons
        case status
        case tvID = "id"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
