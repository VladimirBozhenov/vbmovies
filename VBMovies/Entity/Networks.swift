//
//  Networks.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Networks: Codable {
    
    let name: String
    let networkID: Int?
    let logoPath: String?
    let originCountry: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case networkID
        case logoPath = "logo_path"
        case originCountry = "origin_country"
    }
}
