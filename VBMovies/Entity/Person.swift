//
//  Person.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Person: Codable {
    let castID: Int?
    let character: String?
    let creditID: String
    let department: String?
    let gender: Int?
    let job: String?
    let name: String
    let order: Int?
    let personID: Int
    let profilePath: String?
    
    enum CodingKeys: String, CodingKey {
        case castID = "cast_id"
        case character
        case creditID = "credit_id"
        case department
        case gender
        case job
        case name
        case order
        case personID = "id"
        case profilePath = "profile_path"
    }
}
