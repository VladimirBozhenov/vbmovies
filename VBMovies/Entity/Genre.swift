//
//  Genre.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Genre: Codable {
    let genreID: Int
    let genreName: String
    
    enum CodingKeys: String, CodingKey {
        case genreID = "id"
        case genreName = "name"
    }
}
