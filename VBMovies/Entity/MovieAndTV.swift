//
//  MovieAndTV.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct MovieAndTV: Codable {
    let adult: Bool?
    let backdropPath: String?
    let firstAirDate: String?
    let genreIDs: [Int]
    let mediaType: String?
    let itemID: Int
    let name: String?
    let overview: String
    let originalLanguage: String
    let originalName: String?
    let originalTitle: String?
    let popularity: Double
    let posterPath: String?
    let releaseDate: String?
    let title: String?
    let video: Bool?
    let voteCount: Int
    let voteAverage: Double
        
    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case firstAirDate = "first_air_date"
        case genreIDs = "genre_ids"
        case mediaType = "media_type"
        case itemID = "id"
        case name
        case overview
        case originalLanguage = "original_language"
        case originalName = "original_name"
        case originalTitle = "original_title"
        case popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
        case video
        case voteCount = "vote_count"
        case voteAverage = "vote_average"
    }
}
