//
//  TabBarItems.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct TabBarItems {
    static var items: [String] = ["Movies",
                                  "TV Shows"]
}
