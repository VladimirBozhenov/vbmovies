//
//  SegmentedControlItems.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct SegmentedControlItems {
    static var movieItems: [String] = ["Top Raited",
                                       "Popular",
                                       "Upcoming",
                                       "Now Playing"]
    
    static var tvItems: [String] = ["Top Raited",
                                    "Popular",
                                    "On the Air",
                                    "Airing Today"]
}
