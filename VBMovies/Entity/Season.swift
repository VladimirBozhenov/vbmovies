//
//  Season.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct Season: Codable {
    let airDate: String?
    let episodeCount: Int
    let seasonID: Int
    let name: String
    let overview: String
    let posterPath: String?
    let seasonNumber: Int
    
    enum CodingKeys: String, CodingKey {
        case airDate = "air_date"
        case episodeCount = "episode_count"
        case seasonID = "id"
        case name
        case overview
        case posterPath = "poster_path"
        case seasonNumber = "season_number"
    }
}
