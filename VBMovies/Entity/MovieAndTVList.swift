//
//  MovieAndTVList.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

struct MovieAndTVList: Codable {
    let page: Int
    let results: [MovieAndTV]
    let totalResults: Int
    let totalPages: Int
    
    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
}
