//
//  AppDelegate.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  let window = UIWindow()
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let initialViewController = TopViewController()
    let navigationViewController = UINavigationController(rootViewController: initialViewController)
    window.rootViewController = navigationViewController
    window.makeKeyAndVisible()
    return true
  }
}
