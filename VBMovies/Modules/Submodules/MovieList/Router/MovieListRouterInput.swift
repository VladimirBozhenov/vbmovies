//
//  MovieListRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MovieListRouterInput {
    
//    var movieTopPresenterInput: MovieTabPresenterInput! { get set }
    var topModuleInput: TopModuleInput! { get set }

    func viewIsNotReady()
    func viewIsReady()
    func showErrorAlert(with error: Error)
    func showMovieDetailsScreen(with movieID: Int,
                                for mediaType: String)
}
