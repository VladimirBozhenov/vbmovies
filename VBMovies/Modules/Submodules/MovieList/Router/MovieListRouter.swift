//
//  MovieListRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class MovieListRouter {

    // MARK: - Properties
    var topModuleInput: TopModuleInput!
//    var movieTabPresenterInput: MovieTabPresenterInput!
}

// MARK: - MovieListRouterInput
extension MovieListRouter: MovieListRouterInput {
    
    func viewIsNotReady() {
        topModuleInput.turnActivityIndicatorOn()
    }
    
    func viewIsReady() {
        topModuleInput.turnActivityIndicatorOff()
    }
    
    func showErrorAlert(with error: Error) {
        topModuleInput.showErrorAlert(with: error)
    }
    
    func showMovieDetailsScreen(with itemID: Int,
                                for mediaType: String) {
        topModuleInput.showMovieDetailsScreen(with: itemID,
                                              for: mediaType)
    }
}
