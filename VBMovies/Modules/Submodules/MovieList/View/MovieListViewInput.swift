//
//  MovieListViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol MovieListViewInput: class {

    func setupInitialState()
    func handleObtainedMovies(moviesList: [MovieAndTVListViewModel])
    func reloadView()
}
