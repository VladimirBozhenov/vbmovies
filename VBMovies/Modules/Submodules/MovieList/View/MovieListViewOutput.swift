//
//  MovieListViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol MovieListViewOutput {
  
  var router: MovieListRouterInput! { get set }
  
  func obtainMovies(completion: @escaping (Bool) -> Void)
  func willAskForeMoreData()
  func didGetResult()
  func cellDidTapped(with movieID: Int,
                     mediaType: String)
}
