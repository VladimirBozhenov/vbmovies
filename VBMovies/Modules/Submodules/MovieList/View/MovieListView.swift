//
//  MovieListViewController.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class MovieListView: ASTableNode {
  
  // MARK: - Properties
  var output: MovieListViewOutput!
  private var source: [MovieAndTVListViewModel] = []
  
  // MARK: - Init
  override init(style: UITableView.Style) {
    super.init(style: style)
    let moduleConfigurator = MovieListModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput: self)
    setupInitialState()
  }
}

// MARK: - MoviesListViewInput
extension MovieListView: MovieListViewInput {
  func setupInitialState() {
    dataSource = self
    delegate = self
    backgroundColor = .topMainColor
    self.leadingScreensForBatching = 20
  }
  
  func handleObtainedMovies(moviesList: [MovieAndTVListViewModel]) {
    var indexSet = [IndexPath]()
    let newNumberOfRows = source.count + moviesList.count
    for row in source.count ..< newNumberOfRows {
      indexSet.insert(IndexPath(row: row,
                                section: 0),
                      at: 0)
    }
    source.append(contentsOf: moviesList)
    insertRows(at: indexSet,
               with: .automatic)
  }
  
  func reloadView() {
    source.removeAll()
    reloadData()
  }
}

// MARK: - ASTableDataSource
extension MovieListView: ASTableDataSource {
  func tableNode(_ tableNode: ASTableNode,
                 numberOfRowsInSection section: Int) -> Int {
    return source.count
  }
  
  func tableNode(_ tableNode: ASTableNode,
                 nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
    
    let viewModel = source[indexPath.row]
    let cellNodeBlock = { () -> ASCellNode in
      let node = MovieListCellNode(using: viewModel)
      return node
    }
    return cellNodeBlock
  }
}

// MARK: - ASTableDelegate
extension MovieListView: ASTableDelegate {
  func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
    return true
  }
  
  func tableNode(_ tableNode: ASTableNode,
                 willBeginBatchFetchWith context: ASBatchContext) {
    output.willAskForeMoreData()
    output?.obtainMovies { [weak self] _ in
      guard let self = self else { return }
      context.completeBatchFetching(true)
      self.output.didGetResult()
    }
  }
  
  func tableNode(_ tableNode: ASTableNode,
                 didSelectRowAt indexPath: IndexPath) {
    tableNode.deselectRow(at: indexPath, animated: false)
    let itemID = source[indexPath.row].itemID
    let mediaType = source[indexPath.row].mediaType
    output.cellDidTapped(with: itemID,
                         mediaType: mediaType)
  }
}
