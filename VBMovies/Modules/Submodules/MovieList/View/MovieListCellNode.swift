//
//  MovieListCellNode.swift
//  VBMovieDB-VIPER
//
//  Created by Vladimir Bozhenov on 04/10/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class MovieListCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: MovieAndTVListViewModel
  
  // MARK: - Nodes
  private var movieTitleNode: TextView!
  private var movieTitleNodeInput: TextModuleInput!
  
  private var movieImageNode: PosterView!
  private var movieImageNodeInput: PosterModuleInput!
  
  private var backgroundNode: DisplayView!
  private var backgroundNodeInput: DisplayModuleInput!
  
  private var movieVoteNode: TextView!
  private var movieVoteNodeInput: TextModuleInput!
  
  private var movieReleaseDateNode: TextView!
  private var movieReleaseDateNodeInput: TextModuleInput!
  
  private var movieOverviewNode: TextView!
  private var movieOverviewNodeInput: TextModuleInput!
  
  // MARK: - Init
  init(using source: MovieAndTVListViewModel) {
    self.source = source
    super.init()
    backgroundColor = .topMainColor
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    
    // MARK: - MovieTitleNode
    movieTitleNode = TextView()
    movieTitleNodeInput = movieTitleNode.output as? TextModuleInput
    movieTitleNodeInput.text = source.movieTitle
    movieTitleNodeInput.font = .movieTitleFont
    movieTitleNodeInput.color = .textMainColor
    addSubnode(movieTitleNode)
    
    // MARK: - MovieImageNode
    movieImageNode = PosterView()
    movieImageNode.style.preferredSize = .posterSize
    movieImageNodeInput = movieImageNode.output as? PosterModuleInput
    movieImageNodeInput.poster = source.movieImage
    addSubnode(movieImageNode)
    
    // MARK: - BackgroundNode
    backgroundNode = DisplayView()
    backgroundNodeInput = backgroundNode.output as? DisplayModuleInput
    backgroundNodeInput.borderColor = source.voteBorderColor
    addSubnode(backgroundNode)
    
    // MARK: - MovieVoteNode
    movieVoteNode = TextView()
    movieVoteNodeInput = movieVoteNode.output as? TextModuleInput
    movieVoteNodeInput.text = source.voteAverage
    movieVoteNodeInput.font = .movieVoteFont
    movieVoteNodeInput.color = .textMainColor
    addSubnode(movieVoteNode)
    
    // MARK: - MovieReleaseDateNode
    movieReleaseDateNode = TextView()
    movieReleaseDateNodeInput = movieReleaseDateNode.output as? TextModuleInput
    movieReleaseDateNodeInput.text = source.releaseDate
    movieReleaseDateNodeInput.font = .movieReleaseFont
    movieReleaseDateNodeInput.color = .textMainColor
    addSubnode(movieReleaseDateNode)
    
    // MARK: - MovieOverviewNode
    movieOverviewNode = TextView()
    movieOverviewNodeInput = movieOverviewNode.output as? TextModuleInput
    movieOverviewNodeInput.text = source.overview
    movieOverviewNodeInput.font = .movieOverviewFont
    movieOverviewNodeInput.color = .textMainColor
    addSubnode(movieOverviewNode)
  }
  
  // MARK: - Layout Specs
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let inset = CGFloat(12)
    
    let voteWithBackgroundSpec = ASOverlayLayoutSpec(
      child: backgroundNode,
      overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity,
                                                      left: 9,
                                                      bottom: 14,
                                                      right: 0),
                                 child: movieVoteNode))
    
    let voteAndDateHorizontalSpec = ASStackLayoutSpec(direction: .horizontal,
                                                      spacing: inset,
                                                      justifyContent: .center,
                                                      alignItems: .center,
                                                      children: [voteWithBackgroundSpec, movieReleaseDateNode])
    
    let detailsVerticalSpec = ASStackLayoutSpec(direction: .vertical,
                                                spacing: inset,
                                                justifyContent: .end,
                                                alignItems: .start,
                                                children: [movieTitleNode, voteAndDateHorizontalSpec])
    detailsVerticalSpec.style.height = ASDimension(unit: .points,
                                                   value: CGSize.posterSize.height)
    detailsVerticalSpec.style.flexShrink = 1
    
    let detailsAndImageHorizontalSpec = ASStackLayoutSpec(direction: .horizontal,
                                                          spacing: inset,
                                                          justifyContent: .start,
                                                          alignItems: .start,
                                                          children: [movieImageNode,
                                                                     detailsVerticalSpec])
    
    let fullVerticalSpec = ASStackLayoutSpec(direction: .vertical,
                                             spacing: inset,
                                             justifyContent: .start,
                                             alignItems: .start,
                                             children: [detailsAndImageHorizontalSpec,
                                                        movieOverviewNode])
    
    let insets = UIEdgeInsets(top: inset,
                              left: inset,
                              bottom: inset,
                              right: inset)
    let fullCellWithInsetSpec = ASInsetLayoutSpec(insets: insets,
                                                  child: fullVerticalSpec)
    
    return fullCellWithInsetSpec
  }
}
