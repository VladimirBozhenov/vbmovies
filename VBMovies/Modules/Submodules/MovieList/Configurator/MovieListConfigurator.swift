//
//  MovieListConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class MovieListModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? MovieListView {
            configure(view: view)
        }
    }

    private func configure(view: MovieListView) {
        
        let router = MovieListRouter()

        let presenter = MovieListPresenter()
        presenter.view = view
        presenter.router = router

        let networkService = AlamofireNetworkService()
//        let networkService = URLSessionNetworkService()
        let interactor = MovieListInteractor()
        interactor.output = presenter
        interactor.networkService = networkService

        presenter.interactor = interactor
        view.output = presenter
    }

}
