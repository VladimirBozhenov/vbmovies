//
//  MovieListPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class MovieListPresenter {
  
  // MARK: - Properties
  weak var view: MovieListViewInput!
  var interactor: MovieListInteractorInput!
  var router: MovieListRouterInput!
  
  // MARK: - MovieListInteractorOutput properties
  var state: Path = .movieTop
  var nextPage: Int = 1
}

// MARK: - MoviesListViewOutput
extension MovieListPresenter: MovieListViewOutput {
  func obtainMovies(completion: @escaping (Bool) -> Void) {
    interactor?.loadMovies { completion($0) }
  }
  
  func willAskForeMoreData() {
    router.viewIsNotReady()
  }
  
  func didGetResult() {
    router.viewIsReady()
  }
  
  func cellDidTapped(with itemID: Int,
                     mediaType: String) {
    router.showMovieDetailsScreen(with: itemID,
                                  for: mediaType)
  }
}

// MARK: - MoviesListModuleInput
extension MovieListPresenter: MovieListModuleInput {
  func updateView(with state: Path) {
    self.state = state
    nextPage = 1
    view.reloadView()
  }
}

// MARK: - MovieListInteractorOutput
extension MovieListPresenter: MovieListInteractorOutput {
  func didLoadMovies(movies: [MovieAndTVListViewModel]) {
    DispatchQueue.main.async {
      self.view?.handleObtainedMovies(moviesList: movies)
    }
  }
  
  func showErrorAlert(with error: Error) {
    router.showErrorAlert(with: error)
  }
}
