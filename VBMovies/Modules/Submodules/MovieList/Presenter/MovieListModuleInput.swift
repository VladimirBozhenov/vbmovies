//
//  MovieListModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol MovieListModuleInput: class {

    func updateView(with state: Path)
}
