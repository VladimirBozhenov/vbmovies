//
//  MovieListInteractor.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class MovieListInteractor {
  
  // MARK: - Properties
  weak var output: MovieListInteractorOutput!
  var networkService: NetworkService!
}

// MARK: - MovieListInteractorInput
extension MovieListInteractor: MovieListInteractorInput {
  func loadMovies(completion: @escaping (Bool) -> Void) {
    guard let output = output else {
      completion(false)
      return
    }
    networkService.fetchData(of: MovieAndTVList.self,
                             path: output.state,
                             appendix: nil,
                             ending: nil,
                             page: "\(output.nextPage)") { result in
                              switch result {
                              case .success(let movies):
                                output.nextPage = movies.page + 1
                                let movieList = movies.results.compactMap { MovieAndTVListViewModel(using: $0) }
                                output.didLoadMovies(movies: movieList)
                                completion(true)
                              case .failure(let error):
                                output.showErrorAlert(with: error)
                                completion(false)
                              }
    }
  }
}
