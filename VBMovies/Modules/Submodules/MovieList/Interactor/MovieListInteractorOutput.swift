//
//  MovieListInteractorOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 15/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MovieListInteractorOutput: class {
    
    var state: Path { get set }
    var nextPage: Int { get set }
    
    func didLoadMovies(movies: [MovieAndTVListViewModel])
    func showErrorAlert(with error: Error)
}
