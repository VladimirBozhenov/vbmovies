//
//  StateModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 21.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol StateModuleInput: class {

    func changeSegmentedControlItems(to type: Int)
    
}
