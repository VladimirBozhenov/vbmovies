//
//  StatePresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class StatePresenter {
  
  // MARK: - Properties
  weak var view: StateViewInput!
  var router: StateRouterInput!
}

// MARK: - StateViewOutput
extension StatePresenter: StateViewOutput {
  func segmentDidChange(to index: Int) {
    router.segmentDidCange(to: index)
  }
}

// MARK: - StateModuleInput
extension StatePresenter: StateModuleInput {
  func changeSegmentedControlItems(to type: Int) {
    view.changeSegmentedControlItems(to: type)
  }
}
