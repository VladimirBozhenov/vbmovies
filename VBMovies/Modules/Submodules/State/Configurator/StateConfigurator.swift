//
//  StateConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class StateModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? StateView {
            configure(view: view)
        }
    }

    private func configure(view: StateView) {

        let router = StateRouter()

        let presenter = StatePresenter()
        presenter.view = view
        presenter.router = router
                
        view.output = presenter
    }

}
