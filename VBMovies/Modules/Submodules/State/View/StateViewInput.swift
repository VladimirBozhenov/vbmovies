//
//  StateViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol StateViewInput: class {
    
    func changeSegmentedControlItems(to type: Int)
}
