//
//  StateView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class StateView: UISegmentedControl {
  
  // MARK: - Properties
  var output: StateViewOutput!
  
  // MARK: - Init
  override init(items: [Any]?) {
    super.init(items: items)
    let moduleConfigurator = StateModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput: self)
    setupInitialState()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Methods
  @objc private func segmentDidChange() {
    let newSegmentIndex = self.selectedSegmentIndex
    output.segmentDidChange(to: newSegmentIndex)
  }
  
  private func setupInitialState() {
    self.selectedSegmentIndex = 0
    self.backgroundColor = .topMainColor
    self.tintColor = .clear
    
    self.setTitleTextAttributes([
      NSAttributedString.Key.font: UIFont(font: .SFUIText, weight: .bold, size: 13) as Any,
      NSAttributedString.Key.foregroundColor: UIColor.darkGray
    ], for: .normal)
    
    self.setTitleTextAttributes([
      NSAttributedString.Key.font: UIFont(font: .SFUIText, weight: .bold, size: 13) as Any,
      NSAttributedString.Key.foregroundColor: UIColor.green
    ], for: .selected)
    
    self.addTarget(self,
                   action: #selector(segmentDidChange),
                   for: .valueChanged)
  }
}

// MARK: - StateViewInput
extension StateView: StateViewInput {
  
  func changeSegmentedControlItems(to type: Int) {
    let selectedSegment = selectedSegmentIndex
    removeAllSegments()
    var items: [String] = []
    switch type {
    case 0:
      items = SegmentedControlItems.movieItems
    case 1:
      items = SegmentedControlItems.tvItems
    default:
      break
    }
    for index in 0 ..< items.count {
      insertSegment(withTitle: items[index].description,
                    at: index,
                    animated: false)
    }
    selectedSegmentIndex = selectedSegment
  }
}
