//
//  StateViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol StateViewOutput {
    
    var router: StateRouterInput! { get set }
    
    func segmentDidChange(to index: Int)
}
