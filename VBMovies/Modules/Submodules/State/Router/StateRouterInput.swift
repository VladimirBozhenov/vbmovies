//
//  StateRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol StateRouterInput {
    
//    var tVTabPresenterInput: TVTabPresenterInput! { get set }
//    var movieTabPresenterInput: MovieTabPresenterInput! { get set }
    var topModuleInput: TopModuleInput! { get set }
    func segmentDidCange(to index: Int)

}
