//
//  StateRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 14/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class StateRouter {
    
    // MARK: - Properties
    //    var movieTabPresenterInput: MovieTabPresenterInput!
    var topModuleInput: TopModuleInput!
}

// MARK: - StateRouterInput
extension StateRouter: StateRouterInput {
    func segmentDidCange(to index: Int) {
        topModuleInput.stateDidChanged(to: index)
    }
}
