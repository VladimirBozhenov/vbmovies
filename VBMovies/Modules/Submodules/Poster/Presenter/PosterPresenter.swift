//
//  PosterPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class PosterPresenter: PosterViewOutput {

    // MARK: - Properties
    weak var view: PosterViewInput!
    
    // MARK: - PosterModuleInput Properties
    var poster: URL? {
        didSet {
            updateView()
        }
    }
}

// MARK: - PosterModuleInput
extension PosterPresenter: PosterModuleInput {
    func updateView() {
        guard let poster = poster,
            let view = view else { return }
        view.configureView(with: poster)
    }
}
