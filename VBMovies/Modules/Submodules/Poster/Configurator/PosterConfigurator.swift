//
//  PosterPosterConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class PosterModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? PosterView {
            configure(view: view)
        }
    }

    private func configure(view: PosterView) {

        let presenter = PosterPresenter()
        presenter.view = view

        view.output = presenter
    }

}
