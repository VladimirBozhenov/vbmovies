//
//  PosterView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PosterView: ASNetworkImageNode {

    // MARK: - Properties
    var output: PosterViewOutput!
    
    override init(cache: ASImageCacheProtocol?,
                  downloader: ASImageDownloaderProtocol) {
        super.init(cache: cache,
                   downloader: downloader)
        let moduleConfigurator = PosterModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
    }
}

// MARK: - PosterViewInput
extension PosterView: PosterViewInput {
    func configureView(with poster: URL) {
        url = poster
        clipsToBounds = true
        shouldRenderProgressImages = true
        contentMode = .scaleAspectFill
        cornerRadius = 10
    }
}
