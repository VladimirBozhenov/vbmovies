//
//  DisplayConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class DisplayModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? DisplayView {
            configure(view: view)
        }
    }

    private func configure(view: DisplayView) {

        let presenter = DisplayPresenter()
        presenter.view = view
        
        view.output = presenter
    }

}
