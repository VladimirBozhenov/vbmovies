//
//  DisplayDisplayPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class DisplayPresenter: DisplayViewOutput {

    // MARK: - Properties
    weak var view: DisplayViewInput!
    
    // MARK: - DisplayModuleInput Properties
    var borderColor: CGColor? {
        didSet {
            updateView()
        }
    }
}

// MARK: - DisplayModuleInput
extension DisplayPresenter: DisplayModuleInput {
    func updateView() {
        guard let borderColor = borderColor,
            let view = view else { return }
        view.configureView(with: borderColor)
    }
}
