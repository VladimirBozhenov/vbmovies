//
//  DisplayModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol DisplayModuleInput: class {

    var borderColor: CGColor? { get set }
    
    func updateView()
}
