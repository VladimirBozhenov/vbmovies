//
//  DisplayView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class DisplayView: ASDisplayNode {
  
  // MARK: - Properties
  var output: DisplayViewOutput!
  
  override init() {
    super.init()
    let moduleConfigurator = DisplayModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput: self)
  }
}

// MARK: - DisplayViewInput
extension DisplayView: DisplayViewInput {
  func configureView(with color: CGColor) {
    style.preferredSize = .backgroundSize
    cornerRadius = CGSize.backgroundSize.height / 2
    borderWidth = 4
    borderColor = color
  }
}
