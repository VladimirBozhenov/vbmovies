//
//  TextView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class TextView: ASTextNode {
  
  // MARK: - Properties
  var output: TextViewOutput!
  
  override init() {
    super.init()
    let moduleConfigurator = TextModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput: self)
  }
}

// MARK: - TextViewInput
extension TextView: TextViewInput {
  func configureView(with text: String,
                     font: UIFont,
                     color: UIColor) {
    attributedText = NSAttributedString(
      string: text,
      attributes: [.font: font,
                   .foregroundColor: color])
  }
}
