//
//  TextViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol TextViewInput: class {

    func configureView(with text: String,
                       font: UIFont,
                       color: UIColor)
}
