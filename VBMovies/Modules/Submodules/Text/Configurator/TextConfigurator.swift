//
//  TextConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TextModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? TextView {
            configure(view: view)
        }
    }

    private func configure(view: TextView) {

        let presenter = TextPresenter()
        presenter.view = view
        
        view.output = presenter
    }

}
