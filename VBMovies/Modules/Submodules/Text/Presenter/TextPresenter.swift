//
//  TextPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TextPresenter: TextViewOutput {

    // MARK: - Properties
    weak var view: TextViewInput!
    
    // MARK: - TextModuleInput Properties
    var text: String? {
        didSet {
            updateView()
        }
    }
    var font: UIFont? {
        didSet {
            updateView()
        }
    }
    var color: UIColor? {
        didSet {
            updateView()
        }
    }
}

// MARK: - TextModuleInput
extension TextPresenter: TextModuleInput {
    func updateView() {
        guard let text = text,
            let font = font,
            let color = color,
            let view = view else { return }
        view.configureView(with: text,
                           font: font,
                           color: color)
    }
}
