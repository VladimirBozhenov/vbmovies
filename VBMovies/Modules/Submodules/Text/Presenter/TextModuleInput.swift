//
//  TextModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol TextModuleInput: class {

    var text: String? { get set }
    var font: UIFont? { get set }
    var color: UIColor? { get set }
    
    func updateView()
}
