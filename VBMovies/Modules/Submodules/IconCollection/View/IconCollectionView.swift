//
//  IconCollectionView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class IconCollectionView: ASCollectionNode {

    var output: IconCollectionViewOutput!
    var source: [URL] = []
    
    // MARK: - Init
    override init(frame: CGRect,
                  collectionViewLayout layout: UICollectionViewLayout) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        super.init(frame: frame,
                   collectionViewLayout: layout,
                   layoutFacilitator: nil)
        let moduleConfigurator = IconCollectionModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
        setupInitialState()
    }
}

// MARK: - IconCollectionViewInput
extension IconCollectionView: IconCollectionViewInput {
    func setupInitialState() {
        delegate = self
        dataSource = self
        automaticallyManagesSubnodes = true
        backgroundColor = .clear
    }
    
    func configureView(with icons: [URL]) {
        source = icons
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

// MARK: - ASCollectionDataSource
extension IconCollectionView: ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode,
                        numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode,
                        nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let viewModel = source[indexPath.row]
        let cellNodeBlock = { () -> ASCellNode in
            let node = IconCellNode(using: viewModel)
            return node
        }
        return cellNodeBlock
    }
}

// MARK: - ASCollectionDelegate
extension IconCollectionView: ASCollectionDelegate {
}
