//
//  IconCollectionViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import  Foundation

protocol IconCollectionViewInput: class {

    func setupInitialState()
    func configureView(with icons: [URL])
}
