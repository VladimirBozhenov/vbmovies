//
//  IconCellNode.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class  IconCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: URL
  
  // MARK: - Nodes
  private let iconNode = ASNetworkImageNode()
  
  init(using source: URL) {
    self.source = source
    super.init()
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    automaticallyManagesSubnodes = true
    neverShowPlaceholders = true
    iconNode.url = source
    iconNode.clipsToBounds = true
    iconNode.backgroundColor = .clear
    iconNode.shouldRenderProgressImages = true
    iconNode.contentMode = .scaleAspectFit
    iconNode.style.preferredSize = .iconSise
    addSubnode(iconNode)
  }
  
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let insets = UIEdgeInsets(top: 0,
                              left: 0,
                              bottom: 0,
                              right: 0)
    let spec = ASInsetLayoutSpec(insets: insets,
                                 child: iconNode)
    return spec
  }
}
