//
//  IconCollectionConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class IconCollectionModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? IconCollectionView {
            configure(view: view)
        }
    }

    private func configure(view: IconCollectionView) {

        let router = IconCollectionRouter()

        let presenter = IconCollectionPresenter()
        presenter.view = view
        presenter.router = router

        let interactor = IconCollectionInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        view.output = presenter
    }

}
