//
//  IconCollectionModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

protocol IconCollectionModuleInput: class {
    
    var icons: [URL]? { get set }
    
    func updateView()
}
