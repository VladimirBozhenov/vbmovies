//
//  IconCollectionPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 24/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class IconCollectionPresenter: IconCollectionInteractorOutput {
    
    // MARK: - Properties
    weak var view: IconCollectionViewInput!
    var interactor: IconCollectionInteractorInput!
    var router: IconCollectionRouterInput!
    
    // MARK: - IconCollectionModuleInput Properties
    var icons: [URL]? {
        didSet {
            updateView()
        }
    }
}

// MARK: - IconCollectionModuleInput
extension IconCollectionPresenter: IconCollectionModuleInput {
    func updateView() {
        guard let icons = icons,
            let view = view else { return }
        view.configureView(with: icons)
    }
}

// MARK: - IconCollectionViewOutput
extension IconCollectionPresenter: IconCollectionViewOutput {
    func viewIsReady() {
        
    }
}
