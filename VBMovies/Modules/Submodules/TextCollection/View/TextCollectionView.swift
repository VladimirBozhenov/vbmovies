//
//  TextCollectionView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class TextCollectionView: ASCollectionNode {

    // MARK: - Properties
    var output: TextCollectionViewOutput!
    var source: [String] = []
    
    // MARK: - Init
    override init(frame: CGRect,
                  collectionViewLayout layout: UICollectionViewLayout) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        super.init(frame: frame,
                   collectionViewLayout: layout,
                   layoutFacilitator: nil)
        let moduleConfigurator = TextCollectionModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
        setupInitialState()
    }
}

// MARK: - TextCollectionViewInput
extension TextCollectionView: TextCollectionViewInput {
    
    func setupInitialState() {
        delegate = self
        dataSource = self
        automaticallyManagesSubnodes = true
        backgroundColor = .clear
    }
    
    func configureView(with text: [String]) {
        source = text
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

// MARK: - ASCollectionDataSource
extension TextCollectionView: ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode,
                        numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode,
                        nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let viewModel = source[indexPath.row]
        let cellNodeBlock = { () -> ASCellNode in
            let node = TextCellNode(using: viewModel)
            return node
        }
        return cellNodeBlock
    }
}

// MARK: - ASCollectionDelegate
extension TextCollectionView: ASCollectionDelegate {
}
