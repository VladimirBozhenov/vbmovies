//
//  TextCollectionViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TextCollectionViewInput: class {
    func setupInitialState()
    func configureView(with text: [String])
}
