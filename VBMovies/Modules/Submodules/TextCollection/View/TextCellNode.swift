//
//  TextCellNode.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class TextCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: String
  
  // MARK: - Nodes
  private var textNode: TextView!
  private var textNodeInput: TextModuleInput!
  
  init(using source: String) {
    self.source = source
    super.init()
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    automaticallyManagesSubnodes = true
    neverShowPlaceholders = true
    textNode = TextView()
    textNodeInput = textNode.output as? TextModuleInput
    textNodeInput.text = source
    textNodeInput.font = .genresFont
    textNodeInput.color = .textMainColor
    textNode.backgroundColor = .clear
    addSubnode(textNode)
  }
  
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let insets = UIEdgeInsets(top: 0,
                              left: 8,
                              bottom: 0,
                              right: 8)
    let spec = ASInsetLayoutSpec(insets: insets,
                                 child: textNode)
    return spec
  }
}
