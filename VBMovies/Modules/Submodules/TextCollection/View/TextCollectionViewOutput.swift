//
//  TextCollectionViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TextCollectionViewOutput {

    /**
        @author Vladimir Bozhenov
        Notify presenter that view is ready
    */

    func viewIsReady()
}
