//
//  TextCollectionConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TextCollectionModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? TextCollectionView {
            configure(view: view)
        }
    }

    private func configure(view: TextCollectionView) {

        let router = TextCollectionRouter()

        let presenter = TextCollectionPresenter()
        presenter.view = view
        presenter.router = router

        let interactor = TextCollectionInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        view.output = presenter
    }

}
