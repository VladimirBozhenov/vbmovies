//
//  TextCollectionPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 27/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class TextCollectionPresenter: TextCollectionInteractorOutput {

    // MARK: - Properties
    weak var view: TextCollectionViewInput!
    var interactor: TextCollectionInteractorInput!
    var router: TextCollectionRouterInput!
    
    // MARK: - TextCollectionModuleInput Properties
    var text: [String]? {
        didSet {
            updateView()
        }
    }
}

// MARK: - TextCollectionModuleInput
extension TextCollectionPresenter: TextCollectionModuleInput {
    func updateView() {
        guard let text = text,
            let view = view else { return }
        view.configureView(with: text)
    }
}

// MARK: - TextCollectionViewOutput
extension TextCollectionPresenter: TextCollectionViewOutput {
    func viewIsReady() {
        
    }
}
