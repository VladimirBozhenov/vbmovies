//
//  SearchListPresenter.swift
//  VBMovies
//
//  Created by Евгений Иванов on 23.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class SearchListPresenter {
  
  // MARK: - Properties
  weak var view: SearchListViewInput!
  var interactor: SearchListInteractorInput!
  var router: MovieListRouterInput!
  
  // MARK: - MovieListInteractorOutput properties
  var state: Path = .multiSearch
  var nextPage: Int = 1
}

// MARK: - MoviesListViewOutput
extension SearchListPresenter: SearchListViewOutput {
  
  func obtainObjects(query: String, completion: @escaping (Bool) -> Void) {
    interactor?.search(query: query) { completion($0) }
  }
  
  func willAskForeMoreData() {
    router.viewIsNotReady()
  }
  
  func didGetResult() {
    router.viewIsReady()
  }
  
  func cellDidTapped(with itemID: Int,
                     mediaType: String) {
    router.showMovieDetailsScreen(with: itemID,
                                  for: mediaType)
  }
}

// MARK: - MovieListInteractorOutput
extension SearchListPresenter: SearchListInteractorOutput {
  func didLoadSearchList(searchList: [MovieTvPersonListViewModel]) {
    DispatchQueue.main.async {
      self.view?.handleObtainedMovies(searchList: searchList)
    }
  }
  
  func showErrorAlert(with error: Error) {
    router.showErrorAlert(with: error)
  }
}
