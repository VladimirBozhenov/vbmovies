//
//  SearchListRouterInput.swift
//  VBMovies
//
//  Created by Евгений Иванов on 16.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol SearchListRouterInput {
  
  var topModuleInput: TopModuleInput! { get set }
  
  func viewIsNotReady()
  func viewIsReady()
  func showErrorAlert(with error: Error)
  func showObjectDetailsScreen(with objectID: Int,
                               for mediaType: MediaType)
}
