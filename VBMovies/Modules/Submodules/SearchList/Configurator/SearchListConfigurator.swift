//
//  SearchListConfigurator.swift
//  VBMovies
//
//  Created by Евгений Иванов on 25.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class SearchListConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? SearchListView {
            configure(view: view)
        }
    }

    private func configure(view: SearchListView) {
        
        let router = MovieListRouter()

        let presenter = SearchListPresenter()
        presenter.view = view
        presenter.router = router

        let networkService = AlamofireNetworkService()
        let interactor = SearchListInteractor()
        interactor.output = presenter
        interactor.networkService = networkService

        presenter.interactor = interactor
        view.output = presenter
    }

}
