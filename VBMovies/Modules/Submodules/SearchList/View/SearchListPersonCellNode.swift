//
//  SearchListPersonCellNode.swift
//  VBMovies
//
//  Created by Евгений Иванов on 16.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class SearchListPersonCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: MovieTvPersonListViewModel
  
  // MARK: - Nodes
  private var imageNode: PosterView!
  private var imageNodeInput: PosterModuleInput!
  
  private var titleNode: TextView!
  private var titleNodeInput: TextModuleInput!
  
  private var subtitleNode: TextView!
  private var subtitleNodeInput: TextModuleInput!
  
  private var personDetailsNode: TextView!
  private var personDetailsNodeInput: TextModuleInput!
  
  // MARK: - Init
  init(using source: MovieTvPersonListViewModel) {
    self.source = source
    super.init()
    backgroundColor = .topMainColor
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    
    // MARK: - ImageNode
    imageNode = PosterView()
    imageNode.style.preferredSize = .searchPosterSize
    imageNodeInput = imageNode.output as? PosterModuleInput
    imageNodeInput.poster = source.imageURL
    addSubnode(imageNode)
    
    // MARK: - TitleNode
    titleNode = TextView()
    titleNodeInput = titleNode.output as? TextModuleInput
    titleNodeInput.text = source.name
    titleNodeInput.font = .movieTitleFont
    titleNodeInput.color = .textMainColor
    addSubnode(titleNode)
    
    // MARK: - PersonDetailsNode
    personDetailsNode = TextView()
    personDetailsNodeInput = personDetailsNode.output as? TextModuleInput
    personDetailsNodeInput.text = source.getJobs()
    personDetailsNodeInput.font = .genresFont
    personDetailsNodeInput.color = .textMainColor
    addSubnode(personDetailsNode)
  }
  
  // MARK: - Layout Specs
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let inset = CGFloat(12)
    
    let allTextsSpec = ASStackLayoutSpec(direction: .vertical,
                                         spacing: inset,
                                         justifyContent: .spaceBetween,
                                         alignItems: .start,
                                         children: [titleNode, personDetailsNode])
    allTextsSpec.style.height = ASDimension(unit: .points,
                                            value: CGSize.searchPosterSize.height)
    
    let textsAndImage = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: inset,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [imageNode, allTextsSpec])
    
    let insets = UIEdgeInsets(top: inset,
                              left: inset,
                              bottom: inset,
                              right: inset)
    let fullCellWithInsetSpec = ASInsetLayoutSpec(insets: insets,
                                                  child: textsAndImage)
    
    return fullCellWithInsetSpec
  }
}
