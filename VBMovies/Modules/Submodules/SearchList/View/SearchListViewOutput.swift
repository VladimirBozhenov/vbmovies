//
//  SearchListViewOutput.swift
//  VBMovies
//
//  Created by Евгений Иванов on 25.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

protocol SearchListViewOutput {
  
  var router: MovieListRouterInput! { get set }
  
  func obtainObjects(query: String, completion: @escaping (Bool) -> Void)
  func willAskForeMoreData()
  func didGetResult()
  func cellDidTapped(with itemID: Int,
                     mediaType: String)
}
