//
//  SearchListMovieCellNode.swift
//  VBMovies
//
//  Created by Евгений Иванов on 14.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class SearchListMovieCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: MovieTvPersonListViewModel
  
  // MARK: - Nodes
  private var imageNode: PosterView!
  private var imageNodeInput: PosterModuleInput!
  
  private var titleNode: TextView!
  private var titleNodeInput: TextModuleInput!
  
  private var subtitleNode: TextView!
  private var subtitleNodeInput: TextModuleInput!
  
  private var backgroundNode: DisplayView!
  private var backgroundNodeInput: DisplayModuleInput!
  
  private var movieVoteNode: TextView!
  private var movieVoteNodeInput: TextModuleInput!
  
  // MARK: - Init
  init(using source: MovieTvPersonListViewModel) {
    self.source = source
    super.init()
    backgroundColor = .topMainColor
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    
    // MARK: - ImageNode
    imageNode = PosterView()
    imageNode.style.preferredSize = .searchPosterSize
    imageNodeInput = imageNode.output as? PosterModuleInput
    imageNodeInput.poster = source.imageURL
    addSubnode(imageNode)
    
    // MARK: - TitleNode
    titleNode = TextView()
    titleNodeInput = titleNode.output as? TextModuleInput
    titleNodeInput.text = source.name
    titleNodeInput.font = .movieTitleFont
    titleNodeInput.color = .textMainColor
    addSubnode(titleNode)
    
    // MARK: - SubititleNode
    subtitleNode = TextView()
    subtitleNodeInput = subtitleNode.output as? TextModuleInput
    subtitleNodeInput.text = source.getSubtitle()
    subtitleNodeInput.font = .movieSubtitleFont
    subtitleNodeInput.color = .textMainColor
    addSubnode(subtitleNode)
    
    // MARK: - BackgroundNode
    backgroundNode = DisplayView()
    backgroundNodeInput = backgroundNode.output as? DisplayModuleInput
    backgroundNodeInput.borderColor = source.voteBorderColor
    addSubnode(backgroundNode)
    
    // MARK: - MovieVoteNode
    movieVoteNode = TextView()
    movieVoteNodeInput = movieVoteNode.output as? TextModuleInput
    movieVoteNodeInput.text = source.voteAverage
    movieVoteNodeInput.font = .movieVoteFont
    movieVoteNodeInput.color = .textMainColor
    addSubnode(movieVoteNode)
    
  }
  
  // MARK: - Layout Specs
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let inset = CGFloat(12)
    
    let titleAndSubtitleSpec = ASStackLayoutSpec(direction: .vertical,
                                                 spacing: inset,
                                                 justifyContent: .center,
                                                 alignItems: .start,
                                                 children: [titleNode, subtitleNode])
    
    let voteWithBackgroundSpec = ASOverlayLayoutSpec(
      child: backgroundNode,
      overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity,
                                                      left: 9,
                                                      bottom: 14,
                                                      right: 0),
                                 child: movieVoteNode))
    
    let allTextsSpec = ASStackLayoutSpec(direction: .vertical,
                                         spacing: inset,
                                         justifyContent: .spaceBetween,
                                         alignItems: .start,
                                         children: [titleAndSubtitleSpec, voteWithBackgroundSpec])
    allTextsSpec.style.height = ASDimension(unit: .points,
                                            value: CGSize.searchPosterSize.height)
    
    let textsAndImage = ASStackLayoutSpec(direction: .horizontal,
                                          spacing: inset,
                                          justifyContent: .start,
                                          alignItems: .start,
                                          children: [imageNode, allTextsSpec])
    
    let insets = UIEdgeInsets(top: inset,
                              left: inset,
                              bottom: inset,
                              right: inset)
    let fullCellWithInsetSpec = ASInsetLayoutSpec(insets: insets,
                                                  child: textsAndImage)
    
    return fullCellWithInsetSpec
  }
}
