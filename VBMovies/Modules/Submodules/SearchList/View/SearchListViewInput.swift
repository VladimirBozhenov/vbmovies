//
//  SearchListViewInput.swift
//  VBMovies
//
//  Created by Евгений Иванов on 16.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol SearchListViewInput: class {
  
  func setupInitialState()
  func handleObtainedMovies(searchList: [MovieTvPersonListViewModel])
  func reloadView()
}
