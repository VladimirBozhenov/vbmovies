//
//  SearchListView.swift
//  VBMovies
//
//  Created by Евгений Иванов on 16.01.2020.
//  Copyright © 2020 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import AsyncDisplayKit

class SearchListView: ASTableNode {
  
  // MARK: - Properties
  var output: SearchListViewOutput!
  private var source: [MovieTvPersonListViewModel] = []
  
  // MARK: - Init
  override init(style: UITableView.Style) {
    super.init(style: style)
    let moduleConfigurator = MovieListModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput: self)
    setupInitialState()
  }
}

// MARK: - MoviesListViewInput
extension SearchListView: SearchListViewInput {
  func setupInitialState() {
    dataSource = self
    delegate = self
    backgroundColor = .topMainColor
    self.leadingScreensForBatching = 20
  }
  
  func handleObtainedMovies(searchList: [MovieTvPersonListViewModel]) {
    var indexSet = [IndexPath]()
    let newNumberOfRows = source.count + searchList.count
    for row in source.count ..< newNumberOfRows {
      indexSet.insert(IndexPath(row: row,
                                section: 0),
                      at: 0)
    }
    source.append(contentsOf: searchList)
    insertRows(at: indexSet,
               with: .automatic)
  }
  
  func reloadView() {
    source.removeAll()
    reloadData()
  }
}

// MARK: - ASTableDataSource
extension SearchListView: ASTableDataSource {
  func tableNode(_ tableNode: ASTableNode,
                 numberOfRowsInSection section: Int) -> Int {
    return source.count
  }
  
  func tableNode(_ tableNode: ASTableNode,
                 nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
    
    let viewModel = source[indexPath.row]
    
    let cellNodeBlock = { () -> ASCellNode in
      switch viewModel.mediaType {
      case .movie:
        let node = SearchListMovieCellNode(using: viewModel)
        return node
      case .person:
        let node = SearchListPersonCellNode(using: viewModel)
        return node
      case .tv:
        let node = SearchListMovieCellNode(using: viewModel)
        return node
      }
    }
    return cellNodeBlock
    
  }
}

// MARK: - ASTableDelegate
extension SearchListView: ASTableDelegate {
  
  func shouldBatchFetch(for tableNode: ASTableNode) -> Bool {
    return true
  }
  
  func tableNode(_ tableNode: ASTableNode,
                 willBeginBatchFetchWith context: ASBatchContext) {
    output.willAskForeMoreData()
    output?.obtainObjects(query: "") { [weak self] _ in
      context.completeBatchFetching(true)
      self?.output.didGetResult()
    }
  }
  
  //    func tableNode(_ tableNode: ASTableNode,
  //                   didSelectRowAt indexPath: IndexPath) {
  //        tableNode.deselectRow(at: indexPath, animated: false)
  //        let itemID = source[indexPath.row].itemID
  //        let mediaType = source[indexPath.row].mediaType
  //        output.cellDidTapped(with: itemID,
  //                             mediaType: mediaType)
  //    }
}
