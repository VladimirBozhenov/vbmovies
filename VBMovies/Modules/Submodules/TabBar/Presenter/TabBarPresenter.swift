//
//  TabBarPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class TabBarPresenter: TabBarModuleInput, TabBarInteractorOutput {

    weak var view: TabBarViewInput!
    var interactor: TabBarInteractorInput!
    var router: TabBarRouterInput!

    func viewIsReady() {

    }
}

// MARK: - TabBarViewOutput

extension TabBarPresenter: TabBarViewOutput {
    func typeChanged(to tag: Int) {
        router.changeType(to: tag)
    }
}
