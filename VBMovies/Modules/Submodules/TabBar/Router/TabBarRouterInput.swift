//
//  TabBarRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol TabBarRouterInput {

    var topModuleInput: TopModuleInput! { get set }
    
    func changeType(to type: Int)
}
