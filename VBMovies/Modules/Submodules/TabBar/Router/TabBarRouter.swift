//
//  TabBarRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class TabBarRouter {
    
    // MARK: - Properties
    var topModuleInput: TopModuleInput!
}

// MARK: - TabBarRouterInput
extension TabBarRouter: TabBarRouterInput {
    func changeType(to type: Int) {
        topModuleInput.typeDidChanged(to: type)    }
}
