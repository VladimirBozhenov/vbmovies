//
//  TabBarView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TabBarView: UITabBar {

    var output: TabBarViewOutput!
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        let moduleConfigurator = TabBarModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
        self.delegate = self
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - TabBarViewInput
extension TabBarView: TabBarViewInput {
    func setupInitialState() {
        let movieBarItem = UITabBarItem(title: nil,
                                        image: UIImage(named: "Movie_d.png"),
                                        tag: 0)
        
        let tvBarItem = UITabBarItem(title: nil,
                                     image: UIImage(named: "TV_d.png"),
                                     tag: 1)
        items = [movieBarItem,
                 tvBarItem]
        selectedItem = movieBarItem
    }
}

// MARK: - UITabBarDelegate
extension TabBarView: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar,
                didSelect item: UITabBarItem) {
        output.typeChanged(to: item.tag)
    }
}
