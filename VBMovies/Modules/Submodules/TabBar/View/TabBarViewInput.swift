//
//  TabBarViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TabBarViewInput: class {

    /**
        @author Vladimir Bozhenov
        Setup initial state of the view
    */

    func setupInitialState()
}
