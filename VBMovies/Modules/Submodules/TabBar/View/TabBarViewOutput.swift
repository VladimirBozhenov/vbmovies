//
//  TabBarViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 18/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TabBarViewOutput {

    var router: TabBarRouterInput! { get set }
    
    func typeChanged(to tag: Int)
    func viewIsReady()
}
