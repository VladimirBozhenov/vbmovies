//
//  PosterCollectionViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol PosterCollectionViewInput: class {
    func setupInitialState()
    func configureView(with items: [PosterViewModel])
}
