//
//  PosterCollectionViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol PosterCollectionViewOutput {
    
    var router: PosterCollectionRouterInput! { get set }
    
    func viewIsReady()
    func cellDidTapped(with itemID: Int,
                       mediaType: String)
}
