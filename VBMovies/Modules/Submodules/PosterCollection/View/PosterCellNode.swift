//
//  PosterCellNode.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28.11.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PosterCellNode: ASCellNode {
  
  // MARK: - Properties
  private let source: PosterViewModel
  
  // MARK: - Nodes
  private var posterNode: PosterView!
  private var posterNodeInput: PosterModuleInput!
  
  private let descriptionNode = ASTextNode()
  
  init(using source: PosterViewModel) {
    self.source = source
    super.init()
    setupSubNodes()
  }
  
  // MARK: - SetupNodes
  private func setupSubNodes() {
    automaticallyManagesSubnodes = true
    neverShowPlaceholders = true
    
    posterNode = PosterView()
    posterNode.style.preferredSize = .posterSize
    posterNodeInput = posterNode.output as? PosterModuleInput
    posterNodeInput.poster = source.itemPoster
    addSubnode(posterNode)
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = .center
    descriptionNode.attributedText = NSAttributedString(string: source.itemDescription,
                                                        attributes: [.font: UIFont.movieOverviewFont,
                                                                     .foregroundColor: UIColor.textMainColor,
                                                                     .paragraphStyle: paragraphStyle])
    descriptionNode.style.maxWidth = ASDimension(unit: .points,
                                                 value: CGSize.posterSize.width)
    addSubnode(descriptionNode)
  }
  
  override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
    let stack = ASStackLayoutSpec(direction: .vertical,
                                  spacing: 2,
                                  justifyContent: .start,
                                  alignItems: .center,
                                  children: [self.posterNode,
                                             self.descriptionNode])
    stack.verticalAlignment = .bottom
    return stack
  }
}
