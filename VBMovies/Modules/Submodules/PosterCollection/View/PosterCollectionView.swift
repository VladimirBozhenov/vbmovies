//
//  PosterCollectionView.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PosterCollectionView: ASCollectionNode {

    // MARK: - Properties
    var output: PosterCollectionViewOutput!
    var source: [PosterViewModel] = []

    // MARK: - Init
    override init(frame: CGRect,
                  collectionViewLayout layout: UICollectionViewLayout) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 8
        super.init(frame: frame,
                   collectionViewLayout: layout,
                   layoutFacilitator: nil)
        let moduleConfigurator = PosterCollectionModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
        setupInitialState()
    }
}

// MARK: - PosterCollectionViewInput
extension PosterCollectionView: PosterCollectionViewInput {
    func setupInitialState() {
        delegate = self
        dataSource = self
        automaticallyManagesSubnodes = true
        backgroundColor = .topMainColor
    }
    
    func configureView(with items: [PosterViewModel]) {
        source = items
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

// MARK: - ASCollectionDataSource
extension PosterCollectionView: ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode,
                        numberOfItemsInSection section: Int) -> Int {
        return source.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode,
                        nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let viewModel = source[indexPath.row]
        let cellNodeBlock = { () -> ASCellNode in
            let node = PosterCellNode(using: viewModel)
            return node
        }
        return cellNodeBlock
    }
}

// MARK: - ASCollectionDelegate
extension PosterCollectionView: ASCollectionDelegate {
    
    func collectionNode(_ collectionNode: ASCollectionNode,
                        didSelectItemAt indexPath: IndexPath) {
        output.cellDidTapped(with: source[indexPath.row].itemID,
                             mediaType: source[indexPath.row].mediaType)
    }
}
