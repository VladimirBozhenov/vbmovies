//
//  PosterCollectionPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class PosterCollectionPresenter: PosterCollectionInteractorOutput {
  
  weak var view: PosterCollectionViewInput!
  var interactor: PosterCollectionInteractorInput!
  var router: PosterCollectionRouterInput!
  
  // MARK: - PosterCollectionModuleInput Properties
  var items: [PosterViewModel]? {
    didSet {
      updateView()
    }
  }
}

// MARK: - PosterCollectionModuleInput
extension PosterCollectionPresenter: PosterCollectionModuleInput {
  func updateView() {
    guard let items = items,
      let view = view else { return }
    view.configureView(with: items)
  }
}

// MARK: - PosterCollectionViewOutput
extension PosterCollectionPresenter: PosterCollectionViewOutput {
  func viewIsReady() {}
  func cellDidTapped(with itemID: Int,
                     mediaType: String) {
    router.cellDidTapped(with: itemID,
                         mediaType: mediaType)
  }
  
}
