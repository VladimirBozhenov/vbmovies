//
//  PosterCollectionRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class PosterCollectionRouter: PosterCollectionRouterInput {

    weak var masterModuleInput: MasterModuleInput!
    
    func cellDidTapped(with itemID: Int,
                       mediaType: String) {
        masterModuleInput.cellDidTapped(with: itemID,
                                        mediaType: mediaType)
    }
}
