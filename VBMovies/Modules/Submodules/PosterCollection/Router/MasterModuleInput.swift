//
//  MasterModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 17.12.2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MasterModuleInput: class {
  
  var itemID: Int? { get set }
  func cellDidTapped(with itemID: Int,
                     mediaType: String)
}
