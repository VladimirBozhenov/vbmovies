//
//  PosterCollectionRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol PosterCollectionRouterInput {
    var masterModuleInput: MasterModuleInput! { get set }
    
    func cellDidTapped(with itemID: Int,
                       mediaType: String)
}
