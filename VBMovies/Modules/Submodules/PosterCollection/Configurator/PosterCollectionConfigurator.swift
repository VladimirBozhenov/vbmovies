//
//  PosterCollectionConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 28/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class PosterCollectionModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? PosterCollectionView {
            configure(view: view)
        }
    }

    private func configure(view: PosterCollectionView) {

        let router = PosterCollectionRouter()

        let presenter = PosterCollectionPresenter()
        presenter.view = view
        presenter.router = router

        let interactor = PosterCollectionInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        view.output = presenter
    }

}
