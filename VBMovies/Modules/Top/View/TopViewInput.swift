//
//  TopViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TopViewInput: class {

    func setupInitialState()
    func startActivityIndicator()
    func stopActivityIndicator()
    func showTitle(title: String)
}
