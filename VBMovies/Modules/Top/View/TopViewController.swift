//
//  TopViewController.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class TopViewController: UIViewController {

    // MARK: - Properties
    var segmentedControl: UISegmentedControl!
    var tableNode: ASTableNode!
    var tabBar: UITabBar!
    private let activityIndicator = UIActivityIndicatorView()
    
    // MARK: - Init
    override init(nibName nibNameOrNil: String?,
                  bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil,
                   bundle: nil)
        let moduleConfigurator = TopModuleConfigurator()
        moduleConfigurator.configureModuleForViewInput(viewInput: self)
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Layout
    override func viewWillLayoutSubviews() {
        let navigationBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (navigationController?.navigationBar.frame.height ?? 0.0)
        guard let segmentedControl = segmentedControl,
            let tableNode = tableNode else { return }
        segmentedControl.frame = CGRect(x: 0,
                                        y: navigationBarHeight,
                                        width: view.bounds.width,
                                        height: 40)
        tableNode.frame = CGRect(x: 0,
                                 y: navigationBarHeight + segmentedControl.frame.height,
                                 width: view.bounds.width,
                                 height: view.bounds.height - navigationBarHeight - segmentedControl.frame.height)
        tabBar.itemPositioning = .automatic
        tabBar.frame = CGRect(x: 0,
                              y: view.bounds.height - 80,
                              width: view.bounds.width,
                              height: 80)
        activityIndicator.frame = CGRect(x: (view.bounds.width / 2) - 20,
                                         y: (view.bounds.height / 2) - 20,
                                         width: 40,
                                         height: 40)
        view.addSubview(segmentedControl)
        view.addSubnode(tableNode)
        view.addSubview(tabBar)
        view.addSubview(activityIndicator)
    }
}

// MARK: - TopViewInput
extension TopViewController: TopViewInput {
    func setupInitialState() {
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        activityIndicator.color = .black
        
        title = TabBarItems.items[0]
        tableNode.enableSubtreeRasterization()
    }
    
    func startActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func showTitle(title: String) {
        self.title = title
    }
}
