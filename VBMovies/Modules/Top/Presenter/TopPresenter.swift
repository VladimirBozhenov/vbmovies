//
//  TopPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class TopPresenter {
    
    // MARK: - Properties
    weak var view: TopViewInput!
    var router: TopRouterInput!
    
    var state: Int = 0 {
        didSet {
            updateView(with: state,
                       type: type)
        }
    }
    var type: Int = 0 {
        didSet {
            updateView(with: state,
            type: type)
            router.typeDidChanged(to: type)
        }
    }
    
    func updateView(with state: Int,
                    type: Int) {
        view.showTitle(title: TabBarItems.items[type])
        switch (state, type) {
        case (0, 0):
            router.updateMovieListView(with: .movieTop)
        case (0, 1):
            router.updateMovieListView(with: .tvTop)
        case (1, 0):
            router.updateMovieListView(with: .moviePopular)
        case (1, 1):
            router.updateMovieListView(with: .tvPopular)
        case (2, 0):
            router.updateMovieListView(with: .movieUpcoming)
        case (2, 1):
            router.updateMovieListView(with: .tvOnTheAir)
        case (3, 0):
            router.updateMovieListView(with: .movieNowPlaying)
        case (3, 1):
            router.updateMovieListView(with: .tvAiringToday)
        default:
            router.updateMovieListView(with: .movieTop)
        }
    }
}

// MARK: - TopModuleInput
extension TopPresenter: TopModuleInput {
    
    func stateDidChanged(to state: Int) {
        self.state = state
    }
    
    func typeDidChanged(to type: Int) {
        self.type = type
    }
    
    func turnActivityIndicatorOn() {
        view.startActivityIndicator()
    }
    
    func turnActivityIndicatorOff() {
        view.stopActivityIndicator()
    }
    
    func showErrorAlert(with error: Error) {
        DispatchQueue.main.async {
            Alert.showAlertWith(title: "ERROR",
                                message: error.localizedDescription,
                                in: self.view)
        }
    }
    
    func showMovieDetailsScreen(with itemID: Int,
                                for mediaType: String) {
        router.showMovieDetailsScreen(with: itemID,
                                      for: mediaType)
    }
}
