//
//  TopModuleInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TopModuleInput: class {

//    func updateView(with index: Int)
    func turnActivityIndicatorOn()
    func turnActivityIndicatorOff()
    func showErrorAlert(with error: Error)
    func showMovieDetailsScreen(with movieID: Int,
                                for mediaType: String)
    func stateDidChanged(to state: Int)
    func typeDidChanged(to type: Int)
}
