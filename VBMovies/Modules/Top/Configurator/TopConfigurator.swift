//
//  TopConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TopModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? TopViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: TopViewController) {

        let router = TopRouter()

        let presenter = TopPresenter()
        presenter.view = viewController
        presenter.router = router
        
        router.transitionHandler = viewController
        
        let stateView = StateView(items: SegmentedControlItems.movieItems)
        viewController.segmentedControl = stateView
        router.stateModuleInput = stateView.output as? StateModuleInput
        stateView.output.router.topModuleInput = presenter
        
        let movieListView = MovieListView()
        viewController.tableNode = movieListView
        router.movieListModuleInput = movieListView.output as? MovieListModuleInput
        movieListView.output.router.topModuleInput = presenter
        
        let tabBar = TabBarView()
        viewController.tabBar = tabBar
        tabBar.output.router.topModuleInput = presenter
    }
    
}
