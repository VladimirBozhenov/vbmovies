//
//  TopRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TopRouter {
    
    // MARK: - Properties
    weak var movieListModuleInput: MovieListModuleInput!
    weak var stateModuleInput: StateModuleInput!
    weak var movieDetailsModuleInput: MasterModuleInput!
    weak var tvDetailsModuleInput: MasterModuleInput!
    weak var transitionHandler: UIViewController!
}

// MARK: - TopRouterInput
extension TopRouter: TopRouterInput {
    func updateMovieListView(with state: Path) {
        movieListModuleInput.updateView(with: state)
    }
    
    func showMovieDetailsScreen(with itemID: Int,
                                for mediaType: String) {
        switch mediaType {
        case "movie":
            let movieDetailsViewController = MovieDetailsViewController()
            movieDetailsModuleInput = movieDetailsViewController.output as? MasterModuleInput
            movieDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(movieDetailsViewController,
                                                                       animated: true)
        case "tv":
            let tvDetailsViewController = TVDetailsViewController()
            tvDetailsModuleInput = tvDetailsViewController.output as? MasterModuleInput
            tvDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(tvDetailsViewController,
                                                                       animated: true)
        default:
            break
        }
    }
    
    func typeDidChanged(to type: Int) {
        stateModuleInput.changeSegmentedControlItems(to: type)
    }
}
