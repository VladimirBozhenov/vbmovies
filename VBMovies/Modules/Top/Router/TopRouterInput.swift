//
//  TopRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 13/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol TopRouterInput {
    
    func updateMovieListView(with state: Path)
    func showMovieDetailsScreen(with movieID: Int,
                                for mediaType: String)
    func typeDidChanged(to type: Int)
}
