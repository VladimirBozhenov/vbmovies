//
//  MovieDetailsRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MovieDetailsRouterInput {
    
    func updateSubnodes(with movieDetails: MovieDetailsViewModel)
    func updateSubnodes(with cast: [PersonPosterModel],
                        crew: [PersonPosterModel])
    func showDetailsScreen(with itemID: Int,
                           mediaType: String)
}
