//
//  MovieDetailsRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class MovieDetailsRouter {
    
    // MARK: - Properties
    weak var view: MovieDetailsViewInput!
    var movieTitleModuleInput: TextModuleInput!
    var moviePosterModuleInput: PosterModuleInput!
    var backdropImageModuleInput: PosterModuleInput!
    var movieOverviewModuleInput: TextModuleInput!
    var voteBackgroundModuleInput: DisplayModuleInput!
    var movieVoteModuleInput: TextModuleInput!
    var movieReleaseDateModuleInput: TextModuleInput!
    var movieRuntimeModuleInput: TextModuleInput!
    var countryIconsModuleInput: IconCollectionModuleInput!
    var companyIconsModuleInput: IconCollectionModuleInput!
    var genresModuleInput: TextCollectionModuleInput!
    var castModuleInput: PosterCollectionModuleInput!
    var crewModuleInput: PosterCollectionModuleInput!
    
    weak var personDetailsModuleInput: MasterModuleInput!
    weak var transitionHandler: UIViewController!
}

// MARK: - MovieDetailsRouterInput
extension MovieDetailsRouter: MovieDetailsRouterInput {
    func updateSubnodes(with movieDetails: MovieDetailsViewModel) {
        movieTitleModuleInput.text = movieDetails.movieTitle
        movieTitleModuleInput.font = .movieTitleFont
        movieTitleModuleInput.color = .textWhiteColor
        
        moviePosterModuleInput.poster = movieDetails.movieImage
        
        backdropImageModuleInput.poster = movieDetails.backdropImage
        
        movieOverviewModuleInput.text = movieDetails.overview
        movieOverviewModuleInput.font = .movieOverviewFont
        movieOverviewModuleInput.color = .textMainColor
        
        voteBackgroundModuleInput.borderColor = movieDetails.voteBorderColor
        
        movieVoteModuleInput.text = movieDetails.voteAverage
        movieVoteModuleInput.font = .movieVoteFont
        movieVoteModuleInput.color = .textMainColor
        
        movieReleaseDateModuleInput.text = movieDetails.releaseDate
        movieReleaseDateModuleInput.font = .movieReleaseFont
        movieReleaseDateModuleInput.color = .textMainColor
        
        movieRuntimeModuleInput.text = movieDetails.runtime
        movieRuntimeModuleInput.font = .movieReleaseFont
        movieRuntimeModuleInput.color = .textMainColor
        
        countryIconsModuleInput.icons = movieDetails.countriesFlag
        
        companyIconsModuleInput.icons = movieDetails.companies
        
        genresModuleInput.text = movieDetails.genres
    }
    
    func updateSubnodes(with cast: [PersonPosterModel],
                        crew: [PersonPosterModel]) {
        castModuleInput.items = cast
        crewModuleInput.items = crew
    }
    
    func showDetailsScreen(with itemID: Int,
                           mediaType: String) {
        switch mediaType {
        case "person":
            let personDetailsViewController = PersonDetailsViewController()
            personDetailsModuleInput = personDetailsViewController.output as? MasterModuleInput
            personDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(personDetailsViewController, animated: true)
        default:
            break
        }
    }
}
