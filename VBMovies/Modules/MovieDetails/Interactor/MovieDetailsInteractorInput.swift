//
//  MovieDetailsInteractorInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MovieDetailsInteractorInput {

    func loadMovieDetails(with itemID: Int)
}
