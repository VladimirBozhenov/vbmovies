//
//  MovieDetailsInteractorOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol MovieDetailsInteractorOutput: class {
    
    func didLoadMovie(with movieDetails: MovieDetailsViewModel)
    func didloadCredits(with castList: [PersonPosterModel],
                        crewList: [PersonPosterModel])
    func showErrorAlert(with: Error)
}
