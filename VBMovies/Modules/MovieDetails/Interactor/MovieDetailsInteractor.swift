//
//  MovieDetailsInteractor.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class MovieDetailsInteractor {
    
    // MARK: - Properties
    weak var output: MovieDetailsInteractorOutput!
    var networkService: NetworkService!
}

// MARK: - MovieDetailsInteractorInput
extension MovieDetailsInteractor: MovieDetailsInteractorInput {
    func loadMovieDetails(with itemID: Int) {
        guard let output = output else { return }
        
        networkService.fetchData(of: MovieDetails.self,
                                 path: .movieDetails,
                                 appendix: "\(itemID)",
            ending: nil,
            page: "1") { result in
                switch result {
                case .success(let movieDetals):
                    let movieDetailsModel = MovieDetailsViewModel(using: movieDetals)
                    output.didLoadMovie(with: movieDetailsModel)
                case .failure(let error):
                    output.showErrorAlert(with: error)
                }
        }
        
        networkService.fetchData(of: MovieAndTVCredits.self,
                                 path: .movieDetails,
                                 appendix: "\(itemID)",
            ending: "/credits",
            page: "1") { result in
                switch result {
                case .success(let movieCredits):
                    let castList = movieCredits.cast.compactMap { PersonPosterModel(using: $0) }
                    let crewList = movieCredits.crew.compactMap { PersonPosterModel(using: $0) }
                    output.didloadCredits(with: castList,
                                          crewList: crewList)
                case .failure(let error):
                    output.showErrorAlert(with: error)
                }
        }
    }
}
