//
//  MovieDetailsViewController.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class MovieDetailsViewController: UIViewController {
  
  // MARK: - Properties
  var output: MovieDetailsViewOutput!
  private let activityIndicator = UIActivityIndicatorView()
  
  // MARK: - Nodes
  var scrollNode: ASScrollNode!
  var movieTitleNode: ASTextNode!
  var moviePosterNode: ASNetworkImageNode!
  var backdropImageNode: ASNetworkImageNode!
  var voteBackgroundNode: ASDisplayNode!
  var movieVoteNode: ASTextNode!
  var genresLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Genres:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var genresNode: ASCollectionNode!
  var overviewLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Overview:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var movieOverviewNode: ASTextNode!
  var movieReleaseDateNode: ASTextNode!
  var movieRuntimeNode: ASTextNode!
  var countryIconsNode: ASCollectionNode!
  var companyIconsNode: ASCollectionNode!
  var castLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Cast:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var castNode: ASCollectionNode!
  var crewLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Crew:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var crewNode: ASCollectionNode!
  
  // MARK: - Init
  override init(nibName nibNameOrNil: String?,
                bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nil,
               bundle: nil)
    let moduleConfigurator = MovieDetailsModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput:
      self)
    setupInitialState()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubnode(scrollNode)
  }
  
  // MARK: - Layout
  override func viewWillLayoutSubviews() {
    scrollNode.frame = view.bounds
    scrollNode.automaticallyManagesSubnodes = true
    scrollNode.automaticallyManagesContentSize = true
    scrollNode.backgroundColor = .topMainColor
    moviePosterNode.style.preferredSize = CGSize.posterSize
    movieTitleNode.style.preferredSize = CGSize(width: view.bounds.width - CGSize.posterSize.width - 24, height: 100)
    backdropImageNode.style.preferredSize = CGSize(width: view.bounds.width - 16, height: (view.bounds.width - 16) / 4 * 3)
    countryIconsNode.style.preferredSize = CGSize(width: view.bounds.width - CGSize.posterSize.width - 24,
                                                  height: CGSize.iconSise.height)
    companyIconsNode.style.preferredSize = CGSize(width: view.bounds.width - CGSize.posterSize.width - 24,
                                                  height: CGSize.iconSise.height)
    genresNode.style.preferredSize = CGSize(width: view.bounds.width,
                                            height: 40)
    castNode.style.preferredSize = CGSize(width: view.bounds.width,
                                          height: CGSize.posterSize.height * 2)
    crewNode.style.preferredSize = CGSize(width: view.bounds.width,
                                          height: CGSize.posterSize.height * 2)
    
    activityIndicator.frame = CGRect(x: (view.bounds.width / 2) - 20,
                                     y: (view.bounds.height / 2) - 20,
                                     width: 40,
                                     height: 40)
    view.addSubview(activityIndicator)
  }
}

// MARK: - MovieDetailsViewInput
extension MovieDetailsViewController: MovieDetailsViewInput {
  
  func setupInitialState() {
    activityIndicator.color = .black
    
    scrollNode.layoutSpecBlock = { [weak self] node, constrainedSize in
      guard let self = self else { return ASInsetLayoutSpec() }
      
      let inset: CGFloat = 8
      let insets = UIEdgeInsets(top: 200,
                                left: inset,
                                bottom: inset,
                                right: inset)
      
      let voteWithBackgroundSpec = ASOverlayLayoutSpec(
        child: self.voteBackgroundNode,
        overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: CGFloat.infinity,
                                                        left: 9,
                                                        bottom: 14,
                                                        right: 0),
                                   child: self.movieVoteNode))
      
      let datesStack = ASStackLayoutSpec(direction: .horizontal,
                                         spacing: inset,
                                         justifyContent: .spaceBetween,
                                         alignItems: .center,
                                         children: [voteWithBackgroundSpec,
                                                    self.movieReleaseDateNode,
                                                    self.movieRuntimeNode])
      
      let datesAndVoteAndCountryAndCompanyStack =
        ASStackLayoutSpec(direction: .vertical,
                          spacing: inset,
                          justifyContent: .end,
                          alignItems: .start,
                          children: [self.movieTitleNode,
                                     datesStack,
                                     self.countryIconsNode,
                                     self.companyIconsNode])
      
      let detailsStack = ASStackLayoutSpec(direction: .horizontal,
                                           spacing: inset,
                                           justifyContent: .start,
                                           alignItems: .start,
                                           children: [self.moviePosterNode, datesAndVoteAndCountryAndCompanyStack])
      
      let fullStack = ASStackLayoutSpec(direction: .vertical,
                                        spacing: inset,
                                        justifyContent: .start,
                                        alignItems: .start,
                                        children: [detailsStack,
                                                   self.genresLabel,
                                                   self.genresNode,
                                                   self.overviewLabel,
                                                   self.movieOverviewNode,
                                                   self.castLabel,
                                                   self.castNode,
                                                   self.crewLabel,
                                                   self.crewNode])
      
      let someSpec = ASBackgroundLayoutSpec(
        child: fullStack,
        background: ASInsetLayoutSpec(insets: UIEdgeInsets(top: inset - 200,
                                                           left: -inset,
                                                           bottom: CGFloat.infinity,
                                                           right: -inset),
                                      child: self.backdropImageNode))
      
      let fullStackWithInsetSpec = ASInsetLayoutSpec(insets: insets,
                                                     child: someSpec)
      return fullStackWithInsetSpec
    }
  }
  
  func setTitle(with title: String) {
    DispatchQueue.main.async {
      self.title = title
    }
  }
  
  func startActivityIndicator() {
    DispatchQueue.main.async {
      self.activityIndicator.startAnimating()
    }
  }
  
  func stopActivityIndicator() {
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
    }
  }
}
