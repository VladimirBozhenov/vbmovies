//
//  MovieDetailsViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

protocol MovieDetailsViewInput: class {
    
    func setupInitialState()
    func setTitle(with title: String)
    func startActivityIndicator()
    func stopActivityIndicator()
}
