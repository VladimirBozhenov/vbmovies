//
//  MovieDetailsConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class MovieDetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MovieDetailsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MovieDetailsViewController) {

        let router = MovieDetailsRouter()
        router.view = viewController

        let presenter = MovieDetailsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        router.transitionHandler = viewController
        
        let networkService = AlamofireNetworkService()
//        let networkService = URLSessionNetworkService()
        
        let interactor = MovieDetailsInteractor()
        interactor.output = presenter
        interactor.networkService = networkService

        presenter.interactor = interactor
        viewController.output = presenter
        
        // MARK: - Configure Subnodes
        viewController.scrollNode = ASScrollNode()
        
        let movieTitleView = TextView()
        viewController.movieTitleNode = movieTitleView
        router.movieTitleModuleInput = movieTitleView.output as? TextModuleInput
        
        let moviePosterView = PosterView()
        viewController.moviePosterNode = moviePosterView
        router.moviePosterModuleInput = moviePosterView.output as? PosterModuleInput
        
        let backdropImageView = PosterView()
        viewController.backdropImageNode = backdropImageView
        router.backdropImageModuleInput = backdropImageView.output as? PosterModuleInput
        
        let movieOverviewView = TextView()
        viewController.movieOverviewNode = movieOverviewView
        router.movieOverviewModuleInput = movieOverviewView.output as? TextModuleInput
        
        let voteBackgroundView = DisplayView()
        viewController.voteBackgroundNode = voteBackgroundView
        router.voteBackgroundModuleInput = voteBackgroundView.output as? DisplayModuleInput
        
        let movieVoteView = TextView()
        viewController.movieVoteNode = movieVoteView
        router.movieVoteModuleInput = movieVoteView.output as? TextModuleInput
        
        let movieReleaseDateView = TextView()
        viewController.movieReleaseDateNode = movieReleaseDateView
        router.movieReleaseDateModuleInput = movieReleaseDateView.output as? TextModuleInput
        
        let movieRuntimeView = TextView()
        viewController.movieRuntimeNode = movieRuntimeView
        router.movieRuntimeModuleInput = movieRuntimeView.output as? TextModuleInput
        
        let countryIconsView = IconCollectionView(frame: .zero,
                                                  collectionViewLayout: UICollectionViewFlowLayout())
        viewController.countryIconsNode = countryIconsView
        router.countryIconsModuleInput = countryIconsView.output as? IconCollectionModuleInput
        
        let companyIconsView = IconCollectionView(frame: .zero,
                                                  collectionViewLayout: UICollectionViewFlowLayout())
        viewController.companyIconsNode = companyIconsView
        router.companyIconsModuleInput = companyIconsView.output as? IconCollectionModuleInput
        
        let genresView = TextCollectionView(frame: .zero,
                                            collectionViewLayout: UICollectionViewFlowLayout())
        viewController.genresNode = genresView
        router.genresModuleInput = genresView.output as? TextCollectionModuleInput
        
        let castView = PosterCollectionView(frame: .zero,
                                            collectionViewLayout: UICollectionViewFlowLayout())
        viewController.castNode = castView
        router.castModuleInput = castView.output as? PosterCollectionModuleInput
        castView.output.router.masterModuleInput = presenter
        
        let crewView = PosterCollectionView(frame: .zero,
                                            collectionViewLayout: UICollectionViewFlowLayout())
        viewController.crewNode = crewView
        router.crewModuleInput = crewView.output as? PosterCollectionModuleInput
        crewView.output.router.masterModuleInput = presenter
    }
}
