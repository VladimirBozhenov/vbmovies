//
//  MovieDetailsPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 19/11/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import  Foundation

class MovieDetailsPresenter {

    // MARK: - Properties
    weak var view: MovieDetailsViewInput!
    var interactor: MovieDetailsInteractorInput!
    var router: MovieDetailsRouterInput!
    
    // MARK: - MasterModuleInput Properties
    var itemID: Int? {
        didSet {
            viewIsReady()
        }
    }
}

// MARK: - MovieDetailsViewOutput
extension MovieDetailsPresenter: MovieDetailsViewOutput {
    func viewIsReady() {
        guard let itemID = itemID else { return }
        interactor.loadMovieDetails(with: itemID)
        view.startActivityIndicator()
    }
}

// MARK: - MovieDetailsInteractorOutput
extension MovieDetailsPresenter: MovieDetailsInteractorOutput {
    func didLoadMovie(with movieDetails: MovieDetailsViewModel) {
        router.updateSubnodes(with: movieDetails)
        guard let view = view else { return }
        view.setTitle(with: movieDetails.movieTitle)
        view.stopActivityIndicator()
    }
    
    func didloadCredits(with castList: [PersonPosterModel],
                        crewList: [PersonPosterModel]) {
        router.updateSubnodes(with: castList,
                              crew: crewList)
    }
    
    func showErrorAlert(with error: Error) {
        DispatchQueue.main.async {
            Alert.showAlertWith(title: "ERROR",
                                message: error.localizedDescription,
                                in: self.view)
        }
        view.stopActivityIndicator()
    }
}

// MARK: - MasterModuleInput
extension MovieDetailsPresenter: MasterModuleInput {
    func cellDidTapped(with itemID: Int,
                       mediaType: String) {
        router.showDetailsScreen(with: itemID,
                                 mediaType: mediaType)
    }
}
