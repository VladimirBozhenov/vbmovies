//
//  PersonDetailsConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PersonDetailsModuleConfigurator {
  
  func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
    
    if let viewController = viewInput as? PersonDetailsViewController {
      configure(viewController: viewController)
    }
  }
  
  private func configure(viewController: PersonDetailsViewController) {
    
    let router = PersonDetailsRouter()

    let presenter = PersonDetailsPresenter()
    presenter.view = viewController
    presenter.router = router
    
    router.transitionHandler = viewController
    
    let interactor = PersonDetailsInteractor()
    interactor.output = presenter
    
    presenter.interactor = interactor
    viewController.output = presenter
    
    interactor.networkService = AlamofireNetworkService()
    // interactor.networkService = URLSessionNetworkService()
    
    // MARK: - Configure Subnodes
    viewController.scrollNode = ASScrollNode()
    
    let nameView = TextView()
    viewController.nameNode = nameView
    router.nameModuleInput = nameView.output as? TextModuleInput
    
    let personPosterView = PosterView()
    viewController.personPosterNode = personPosterView
    router.personPosterModuleInput = personPosterView.output as? PosterModuleInput
    
    let popularityBackgroundView = DisplayView()
    viewController.popularityBackgroundNode = popularityBackgroundView
    router.popularityBackgroundModuleInput = popularityBackgroundView.output as? DisplayModuleInput
    
    let popularityView = TextView()
    viewController.popularityNode = popularityView
    router.popularityModuleInput = popularityView.output as? TextModuleInput
    
    let placeOfBirthView = TextView()
    viewController.placeOfBirthNode = placeOfBirthView
    router.placeOfBirthModuleInput = placeOfBirthView.output as? TextModuleInput
    
    let birthdayView = TextView()
    viewController.birthdayNode = birthdayView
    router.birthdayModuleInput = birthdayView.output as? TextModuleInput
    
    let deathdayView = TextView()
    viewController.deathdayNode = deathdayView
    router.deathdayModuleInput = deathdayView.output as? TextModuleInput
    
    let biographyView = TextView()
    viewController.biographyNode = biographyView
    router.biographyModuleInput = biographyView.output as? TextModuleInput
    
    let castInView = PosterCollectionView(frame: .zero,
                                          collectionViewLayout: UICollectionViewFlowLayout())
    viewController.castNode = castInView
    router.castModuleInput = castInView.output as? PosterCollectionModuleInput
    castInView.output.router.masterModuleInput = presenter
    
    let crewInView = PosterCollectionView(frame: .zero,
                                          collectionViewLayout: UICollectionViewFlowLayout())
    viewController.crewNode = crewInView
    router.crewModuleInput = crewInView.output as? PosterCollectionModuleInput
    crewInView.output.router.masterModuleInput = presenter
  }
}
