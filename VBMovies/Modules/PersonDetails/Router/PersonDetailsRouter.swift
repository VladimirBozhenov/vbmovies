//
//  PersonDetailsRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PersonDetailsRouter {
    
    // MARK: - Properties
    var nameModuleInput: TextModuleInput!
    var personPosterModuleInput: PosterModuleInput!
    var popularityBackgroundModuleInput: DisplayModuleInput!
    var popularityModuleInput: TextModuleInput!
    var placeOfBirthModuleInput: TextModuleInput!
    var birthdayModuleInput: TextModuleInput!
    var deathdayModuleInput: TextModuleInput!
    var biographyModuleInput: TextModuleInput!
    var castModuleInput: PosterCollectionModuleInput!
    var crewModuleInput: PosterCollectionModuleInput!
    
    weak var movieDetailsModuleInput: MasterModuleInput!
    weak var tvDetailsModuleInput: MasterModuleInput!
    weak var transitionHandler: UIViewController!

}

// MARK: - PersonDetailsRouterInput
extension PersonDetailsRouter: PersonDetailsRouterInput {
    func updateSubnodes(with personDetails: PersonDetailsViewModel) {
        nameModuleInput.text = personDetails.name
        nameModuleInput.font = .movieTitleFont
        nameModuleInput.color = .textMainColor
        
        personPosterModuleInput.poster = personDetails.profilePath
        
        popularityBackgroundModuleInput.borderColor = personDetails.popularityBorderColor
        
        popularityModuleInput.text = personDetails.popularity
        popularityModuleInput.font = .movieVoteFont
        popularityModuleInput.color = .textMainColor
        
        placeOfBirthModuleInput.text = personDetails.placeOfBirth
        placeOfBirthModuleInput.font = .movieReleaseFont
        placeOfBirthModuleInput.color = .textMainColor
        
        birthdayModuleInput.text = personDetails.birthday
        birthdayModuleInput.font = .movieReleaseFont
        birthdayModuleInput.color = .textMainColor
        
        deathdayModuleInput.text = personDetails.deathday
        deathdayModuleInput.font = .movieReleaseFont
        deathdayModuleInput.color = .textMainColor
        
        biographyModuleInput.text = personDetails.biography
        biographyModuleInput.font = .movieOverviewFont
        biographyModuleInput.color = .textMainColor
    }
    
    func updateSubnodes(with castInList: [MoviePosterModel],
                        crewInList: [MoviePosterModel]) {
        castModuleInput.items = castInList
        crewModuleInput.items = crewInList
    }
    
    func showDetailsScreen(with itemID: Int,
                           mediaType: String) {
        switch mediaType {
        case "movie":
            let movieDetailsViewController = MovieDetailsViewController()
            movieDetailsModuleInput = movieDetailsViewController.output as? MasterModuleInput
            movieDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(movieDetailsViewController, animated: true)
        case "tv":
            let tvDetailsViewController = TVDetailsViewController()
            tvDetailsModuleInput = tvDetailsViewController.output as? MasterModuleInput
            tvDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(tvDetailsViewController, animated: true)
        default:
            break
        }
    }
}
