//
//  PersonDetailsRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol PersonDetailsRouterInput {
    
    func updateSubnodes(with personDetails: PersonDetailsViewModel)
    func updateSubnodes(with castInList: [MoviePosterModel],
                        crewInList: [MoviePosterModel])
    func showDetailsScreen(with itemID: Int,
                           mediaType: String)

}
