//
//  PersonDetailsViewInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol PersonDetailsViewInput: class {
    func setupInitialState()
    func startActivityIndicator()
    func stopActivityIndicator()
    func setTitle(title: String)
}
