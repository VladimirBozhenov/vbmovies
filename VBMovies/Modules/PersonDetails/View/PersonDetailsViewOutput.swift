//
//  PersonDetailsViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol PersonDetailsViewOutput {
    func viewIsReady()
}
