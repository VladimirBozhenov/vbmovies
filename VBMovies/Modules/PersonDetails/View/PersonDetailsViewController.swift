//
//  PersonDetailsViewController.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class PersonDetailsViewController: UIViewController {
  
  // MARK: - Properties
  var output: PersonDetailsViewOutput!
  private let activityIndicator = UIActivityIndicatorView()
  
  // MARK: - Nodes
  var scrollNode: ASScrollNode!
  var nameNode: ASTextNode!
  var personPosterNode: ASNetworkImageNode!
  var popularityBackgroundNode: ASDisplayNode!
  var popularityNode: ASTextNode!
  var placeOfBirthLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Place of Birth:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var placeOfBirthNode: ASTextNode!
  var birthdayLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Date of Birth:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var birthdayNode: ASTextNode!
  var deathdayLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Date of Death:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var deathdayNode: ASTextNode!
  var biographyLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Biography:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var biographyNode: ASTextNode!
  var castLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Cast In:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var castNode: ASCollectionNode!
  var crewLabel: ASTextNode = {
    let textNode = ASTextNode()
    textNode.attributedText = NSAttributedString(string: "Crew In:",
                                                 attributes: [.font: UIFont.movieTitleFont,
                                                              .foregroundColor: UIColor.textMainColor])
    return textNode
  }()
  var crewNode: ASCollectionNode!
  
  // MARK: - Init
  override init(nibName nibNameOrNil: String?,
                bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nil,
               bundle: nil)
    let moduleConfigurator = PersonDetailsModuleConfigurator()
    moduleConfigurator.configureModuleForViewInput(viewInput:
      self)
    setupInitialState()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    output.viewIsReady()
    view.addSubnode(scrollNode)
  }
  
  // MARK: - Layout
  override func viewWillLayoutSubviews() {
    scrollNode.frame = view.bounds
    scrollNode.automaticallyManagesSubnodes = true
    scrollNode.automaticallyManagesContentSize = true
    scrollNode.backgroundColor = .topMainColor
    personPosterNode.style.preferredSize = CGSize.posterSize
    castNode.style.preferredSize = CGSize(width: view.bounds.width,
                                          height: CGSize.posterSize.height * 2)
    crewNode.style.preferredSize = CGSize(width: view.bounds.width,
                                          height: CGSize.posterSize.height * 2)
    
    activityIndicator.frame = CGRect(x: (view.bounds.width / 2) - 20,
                                     y: (view.bounds.height / 2) - 20,
                                     width: 40,
                                     height: 40)
    activityIndicator.color = .white
    view.addSubview(activityIndicator)
  }
}

// MARK: - PersonDetailsViewInput
extension PersonDetailsViewController: PersonDetailsViewInput {
  
  func setupInitialState() {
    activityIndicator.color = .black
    
    scrollNode.layoutSpecBlock = { [weak self] node, constrainedSize in
      guard let self = self else { return ASInsetLayoutSpec() }
      
      let inset: CGFloat = 8
      let insets = UIEdgeInsets(top: inset,
                                left: inset,
                                bottom: inset,
                                right: inset)
      
      let datesSpec = ASStackLayoutSpec(direction: .vertical,
                                        spacing: inset,
                                        justifyContent: .spaceBetween,
                                        alignItems: .start,
                                        children: [self.birthdayLabel,
                                                   self.birthdayNode,
                                                   self.placeOfBirthLabel,
                                                   self.placeOfBirthNode,
                                                   self.deathdayLabel,
                                                   self.deathdayNode])
      
      let horizontalSpec = ASStackLayoutSpec(direction: .horizontal,
                                             spacing: inset,
                                             justifyContent: .start,
                                             alignItems: .start,
                                             children: [self.personPosterNode,
                                                        datesSpec])
      
      let fullStack = ASStackLayoutSpec(direction: .vertical,
                                        spacing: inset,
                                        justifyContent: .start,
                                        alignItems: .start,
                                        children: [self.nameNode,
                                                   horizontalSpec,
                                                   self.biographyLabel,
                                                   self.biographyNode,
                                                   self.castLabel,
                                                   self.castNode,
                                                   self.crewLabel,
                                                   self.crewNode])
      
      let fullStackWithInsetSpec = ASInsetLayoutSpec(insets: insets,
                                                     child: fullStack)
      return fullStackWithInsetSpec
    }
  }
  
  func startActivityIndicator() {
    DispatchQueue.main.async {
      self.activityIndicator.startAnimating()
    }
  }
  
  func stopActivityIndicator() {
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
    }
  }
  
  func setTitle(title: String) {
    DispatchQueue.main.async {
      self.title = title
    }
  }
  
}
