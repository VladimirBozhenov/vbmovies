//
//  PersonDetailsInteractor.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class PersonDetailsInteractor {
    
    // MARK: - Properties
    weak var output: PersonDetailsInteractorOutput!
    var networkService: NetworkService!
}

// MARK: - PersonDetailsInteractorInput
extension PersonDetailsInteractor: PersonDetailsInteractorInput {
    func loadItemDetaits(itemID: Int) {
        guard let output = output else {
            return
        }
        networkService.fetchData(of: PersonDetails.self,
                                 path: .personDetails,
                                 appendix: nil,
                                 ending: "\(itemID)",
        page: "1") { result in
            switch result {
            case .success(let personDetals):
                let personDetailsModel = PersonDetailsViewModel(using: personDetals)
                output.didLoadPersonDetails(with: personDetailsModel)
            case .failure(let error):
                output.showErrorAlert(with: error)
            }
        }
        
        networkService.fetchData(of: PersonCredits.self,
                                 path: .personDetails,
                                 appendix: "\(itemID)",
            ending: "/combined_credits",
            page: "1") { result in
                switch result {
                case .success(let personCredits):
                    let castInList = personCredits.castIn.compactMap { MoviePosterModel(using: $0) }
                    let crewInList = personCredits.crewIn.compactMap { MoviePosterModel(using: $0) }
                    output.didloadCredits(with: castInList,
                                          crewInList: crewInList)
                case .failure(let error):
                    output.showErrorAlert(with: error)
                }
        }
    }
}
