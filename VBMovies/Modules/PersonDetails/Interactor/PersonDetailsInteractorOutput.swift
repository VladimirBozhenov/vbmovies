//
//  PersonDetailsInteractorOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol PersonDetailsInteractorOutput: class {
    
    func didLoadPersonDetails(with personDetails: PersonDetailsViewModel)
    func didloadCredits(with castInList: [MoviePosterModel],
                        crewInList: [MoviePosterModel])
    func showErrorAlert(with error: Error)

}
