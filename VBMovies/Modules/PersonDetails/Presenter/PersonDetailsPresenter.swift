//
//  PersonDetailsPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 10/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class PersonDetailsPresenter {
    
    // MARK: - MasterModuleInput Properties
    var itemID: Int?

    // MARK: - Properties
    weak var view: PersonDetailsViewInput!
    var interactor: PersonDetailsInteractorInput!
    var router: PersonDetailsRouterInput!
}

// MARK: - PersonDetailsViewOutput
extension PersonDetailsPresenter: PersonDetailsViewOutput {
    
    func viewIsReady() {
        guard let itemID = itemID else { return }
        interactor.loadItemDetaits(itemID: itemID)
        view.startActivityIndicator()
    }
}

// MARK: - MasterModuleInput
extension PersonDetailsPresenter: MasterModuleInput {
    
    func cellDidTapped(with itemID: Int,
                       mediaType: String) {
        router.showDetailsScreen(with: itemID,
                                 mediaType: mediaType)
    }
    
}

// MARK: - PersonDetailsInteractorOutput
extension PersonDetailsPresenter: PersonDetailsInteractorOutput {
    
    func didLoadPersonDetails(with personDetails: PersonDetailsViewModel) {
        self.router.updateSubnodes(with: personDetails)
        guard let view = view else { return }
        view.setTitle(title: personDetails.name)
        view.stopActivityIndicator()
    }
    
    func didloadCredits(with castInList: [MoviePosterModel],
                        crewInList: [MoviePosterModel]) {
        router.updateSubnodes(with: castInList,
                              crewInList: crewInList)
    }
    
    func showErrorAlert(with error: Error) {
        DispatchQueue.main.async {
            Alert.showAlertWith(title: "ERROR",
                                message: error.localizedDescription,
                                in: self.view)
        }
    }
}

// MARK: - PersonDetailsModuleInput
extension PersonDetailsPresenter: PersonDetailsModuleInput {
    
}
