//
//  TVDetailsPresenter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import  Foundation

class TVDetailsPresenter {
  
  weak var view: TVDetailsViewInput!
  var interactor: TVDetailsInteractorInput!
  var router: TVDetailsRouterInput!
  
  // MARK: - MasterModuleInput Properties
  var itemID: Int? {
    didSet {
      viewIsReady()
    }
  }
}

// MARK: - MasterModuleInput
extension TVDetailsPresenter: MasterModuleInput {
  func cellDidTapped(with itemID: Int,
                     mediaType: String) {
    router.showDetailsScreen(with: itemID,
                             mediaType: mediaType)
  }
}

// MARK: - TVDetailsViewOutput
extension TVDetailsPresenter: TVDetailsViewOutput {
  func viewIsReady() {
    guard let itemID = itemID else { return }
    interactor.loadTVDetails(with: itemID)
    view.startActivityIndicator()
  }
}

// MARK: - TVDetailsInteractorOutput
extension TVDetailsPresenter: TVDetailsInteractorOutput {
  func didLoadTV(with tvDetails: TVDetailsViewModel) {
    router.updateSubnodes(with: tvDetails)
    guard let view = view else { return }
    view.setTitle(with: tvDetails.tvTitle)
    view.stopActivityIndicator()
  }
  
  func didloadCredits(with castList: [PersonPosterModel], crewList: [PersonPosterModel]) {
    router.updateSubnodes(with: castList,
                          crew: crewList)
  }
  
  func showErrorAlert(with error: Error) {
    DispatchQueue.main.async {
      Alert.showAlertWith(title: "ERROR",
                          message: error.localizedDescription,
                          in: self.view)
    }
    view.stopActivityIndicator()
  }
}
