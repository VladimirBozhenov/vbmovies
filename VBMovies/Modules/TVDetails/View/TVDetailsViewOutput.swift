//
//  TVDetailsViewOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

protocol TVDetailsViewOutput {
    func viewIsReady()
}
