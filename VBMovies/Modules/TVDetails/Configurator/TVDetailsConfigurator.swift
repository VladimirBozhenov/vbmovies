//
//  TVDetailsConfigurator.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import AsyncDisplayKit

class TVDetailsModuleConfigurator {
  
  func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
    
    if let viewController = viewInput as? TVDetailsViewController {
      configure(viewController: viewController)
    }
  }
  
  private func configure(viewController: TVDetailsViewController) {
    
    let router = TVDetailsRouter()
    router.view = viewController
    
    let presenter = TVDetailsPresenter()
    presenter.view = viewController
    presenter.router = router
    
    router.transitionHandler = viewController
    
    let networkService = AlamofireNetworkService()
//    let networkService = URLSessionNetworkService()
    
    let interactor = TVDetailsInteractor()
    interactor.output = presenter
    interactor.networkService = networkService
    
    presenter.interactor = interactor
    viewController.output = presenter
    
    // MARK: - Configure Subnodes
    viewController.scrollNode = ASScrollNode()
    
    let tvTitleView = TextView()
    viewController.tvTitleNode = tvTitleView
    router.tvTitleModuleInput = tvTitleView.output as? TextModuleInput
    
    let tvPosterView = PosterView()
    viewController.tvPosterNode = tvPosterView
    router.tvPosterModuleInput = tvPosterView.output as? PosterModuleInput
    
    let backdropImageView = PosterView()
    viewController.backdropImageNode = backdropImageView
    router.backdropImageModuleInput = backdropImageView.output as? PosterModuleInput
    
    let tvOverviewView = TextView()
    viewController.tvOverviewNode = tvOverviewView
    router.tvOverviewModuleInput = tvOverviewView.output as? TextModuleInput
    
    let voteBackgroundView = DisplayView()
    viewController.voteBackgroundNode = voteBackgroundView
    router.voteBackgroundModuleInput = voteBackgroundView.output as? DisplayModuleInput
    
    let tvVoteView = TextView()
    viewController.tvVoteNode = tvVoteView
    router.tvVoteModuleInput = tvVoteView.output as? TextModuleInput
    
    let tvReleaseDateView = TextView()
    viewController.tvReleaseDateNode = tvReleaseDateView
    router.tvReleaseDateModuleInput = tvReleaseDateView.output as? TextModuleInput
    
    let companyIconsView = IconCollectionView(frame: .zero,
                                              collectionViewLayout: UICollectionViewFlowLayout())
    viewController.companyIconsNode = companyIconsView
    router.companyIconsModuleInput = companyIconsView.output as? IconCollectionModuleInput
    
    let networksIconsView = IconCollectionView(frame: .zero,
                                               collectionViewLayout: UICollectionViewFlowLayout())
    viewController.networksIconNode = networksIconsView
    router.networksIconsModuleInput = networksIconsView.output as? IconCollectionModuleInput
    
    let genresView = TextCollectionView(frame: .zero,
                                        collectionViewLayout: UICollectionViewFlowLayout())
    viewController.genresNode = genresView
    router.genresModuleInput = genresView.output as? TextCollectionModuleInput
    
    let castView = PosterCollectionView(frame: .zero,
                                        collectionViewLayout: UICollectionViewFlowLayout())
    viewController.castNode = castView
    router.castModuleInput = castView.output as? PosterCollectionModuleInput
    castView.output.router.masterModuleInput = presenter
    
    let crewView = PosterCollectionView(frame: .zero,
                                        collectionViewLayout: UICollectionViewFlowLayout())
    viewController.crewNode = crewView
    router.crewModuleInput = crewView.output as? PosterCollectionModuleInput
    crewView.output.router.masterModuleInput = presenter
  }
  
}
