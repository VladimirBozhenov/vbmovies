//
//  TVDetailsInteractorOutput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol TVDetailsInteractorOutput: class {

    func didLoadTV(with tvDetails: TVDetailsViewModel)
    func didloadCredits(with castList: [PersonPosterModel],
                        crewList: [PersonPosterModel])
    func showErrorAlert(with: Error)
}
