//
//  TVDetailsInteractor.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

class TVDetailsInteractor {

    // MARK: - Properties
    weak var output: TVDetailsInteractorOutput!
    var networkService: NetworkService!
}

// MARK: - TVDetailsInteractorInput
extension TVDetailsInteractor: TVDetailsInteractorInput {
    func loadTVDetails(with itemID: Int) {
        guard let output = output else { return }
        
        networkService.fetchData(of: TVDetails.self,
                                 path: .tvDetails,
                                 appendix: "\(itemID)",
            ending: nil,
            page: "1") { result in
                switch result {
                case .success(let tvDetals):
                    let tvDetailsModel = TVDetailsViewModel(using: tvDetals)
                    output.didLoadTV(with: tvDetailsModel)
                case .failure(let error):
                    output.showErrorAlert(with: error)
                }
        }
        
        networkService.fetchData(of: MovieAndTVCredits.self,
                                 path: .tvDetails,
                                 appendix: "\(itemID)",
            ending: "/credits",
            page: "1") { result in
                switch result {
                case .success(let tvCredits):
                    let castList = tvCredits.cast.compactMap { PersonPosterModel(using: $0) }
                    let crewList = tvCredits.crew.compactMap { PersonPosterModel(using: $0) }
                    output.didloadCredits(with: castList,
                                          crewList: crewList)
                case .failure(let error):
                    output.showErrorAlert(with: error)
                }
        }
    }
}
