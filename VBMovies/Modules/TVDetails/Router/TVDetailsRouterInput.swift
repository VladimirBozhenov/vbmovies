//
//  TVDetailsRouterInput.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

protocol TVDetailsRouterInput {
    func updateSubnodes(with tvDetails: TVDetailsViewModel)
    func updateSubnodes(with cast: [PersonPosterModel],
                        crew: [PersonPosterModel])
    func showDetailsScreen(with itemID: Int,
                           mediaType: String)
}
