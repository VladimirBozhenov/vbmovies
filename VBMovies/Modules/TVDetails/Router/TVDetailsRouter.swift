//
//  TVDetailsRouter.swift
//  VBMovies
//
//  Created by Vladimir Bozhenov on 20/12/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit

class TVDetailsRouter {

    // MARK: - Properties
    weak var view: TVDetailsViewInput!
    var tvTitleModuleInput: TextModuleInput!
    var tvPosterModuleInput: PosterModuleInput!
    var backdropImageModuleInput: PosterModuleInput!
    var tvOverviewModuleInput: TextModuleInput!
    var voteBackgroundModuleInput: DisplayModuleInput!
    var tvVoteModuleInput: TextModuleInput!
    var tvReleaseDateModuleInput: TextModuleInput!
    var companyIconsModuleInput: IconCollectionModuleInput!
    var networksIconsModuleInput: IconCollectionModuleInput!
    var genresModuleInput: TextCollectionModuleInput!
    var castModuleInput: PosterCollectionModuleInput!
    var crewModuleInput: PosterCollectionModuleInput!
    
    weak var personDetailsModuleInput: MasterModuleInput!
    weak var transitionHandler: UIViewController!
}

extension TVDetailsRouter: TVDetailsRouterInput {
    func updateSubnodes(with tvDetails: TVDetailsViewModel) {
        tvTitleModuleInput.text = tvDetails.tvTitle
        tvTitleModuleInput.font = .movieTitleFont
        tvTitleModuleInput.color = .textWhiteColor
        
        tvPosterModuleInput.poster = tvDetails.tvImage
        
        backdropImageModuleInput.poster = tvDetails.backdropImage
        
        tvOverviewModuleInput.text = tvDetails.overview
        tvOverviewModuleInput.font = .movieOverviewFont
        tvOverviewModuleInput.color = .textMainColor
        
        voteBackgroundModuleInput.borderColor = tvDetails.voteBorderColor
        
        tvVoteModuleInput.text = tvDetails.voteAverage
        tvVoteModuleInput.font = .movieVoteFont
        tvVoteModuleInput.color = .textMainColor
        
        tvReleaseDateModuleInput.text = tvDetails.releaseDate
        tvReleaseDateModuleInput.font = .movieReleaseFont
        tvReleaseDateModuleInput.color = .textMainColor
        
        companyIconsModuleInput.icons = tvDetails.companies
        
        networksIconsModuleInput.icons = tvDetails.networks
        
        genresModuleInput.text = tvDetails.genres
    }
    
    func updateSubnodes(with cast: [PersonPosterModel],
                        crew: [PersonPosterModel]) {
        castModuleInput.items = cast
        crewModuleInput.items = crew
    }
    
    func showDetailsScreen(with itemID: Int,
                           mediaType: String) {
        switch mediaType {
        case "person":
            let personDetailsViewController = PersonDetailsViewController()
            personDetailsModuleInput = personDetailsViewController.output as? MasterModuleInput
            personDetailsModuleInput.itemID = itemID
            transitionHandler.navigationController?.pushViewController(personDetailsViewController, animated: true)
        default:
            break
        }
    }
}
